import requests 
import json
import html
URL = "https://opentdb.com/api.php?amount=50&type=multiple"
with open('questionsData.txt', 'w') as file:  # Use file to refer to the file object
	for i in range (10):
		r = requests.get(url = URL)
		deictj = r.json()
		dictr = deictj['results']
		jsonstat = json.dumps(dictr)
		jsonstat = jsonstat.replace("&quot;", "'")
		decoded_json = html.unescape(jsonstat)
		new_dict = decoded_json.split("}, ")
		for i in range(len(new_dict)):
			new_dict[i] =  new_dict[i] + "}"
		new_dict[-1] = new_dict[-1][:-1]
		for f in new_dict:
			try:
				file.write(f)
				file.write('\n')
			except:
				pass