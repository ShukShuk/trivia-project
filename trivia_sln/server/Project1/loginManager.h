#pragma once
#include <iostream>
#include "Idatabase.h"
#include "LoggedUser.h"
#include <vector>

class loginManager
{
public:
	loginManager(Idatabase* db);
	bool signup(std::string, std::string, std::string);
	bool login(std::string, std::string);
	bool logout(std::string username);
	void findpass(std::string email);
private:
	Idatabase * m_database;
	std::vector<LoggedUser> m_loggedUsers;

};
