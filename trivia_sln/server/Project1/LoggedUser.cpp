#include "LoggedUser.h"

LoggedUser::LoggedUser()
{
}

LoggedUser::LoggedUser(std::string name)
{
	this->m_username = name;
}

std::string LoggedUser::getUserName() const
{
	return m_username;
}



void LoggedUser::setUserName(std::string str)
{
	m_username = str;
}
