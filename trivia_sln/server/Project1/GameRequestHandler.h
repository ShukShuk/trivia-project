#pragma once
#include "IRequestHandler.h"
#include <iostream>
#include "Game.h"
#include "GameManager.h"
#include "LoggedUser.h"
#include "RequestHandleFactory.h"
#include <ctime>
#include "Response.h"
class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(LoggedUser user, GameManager * gManager, Room * r, RequestHandleFactory *rhf);
	Game * m_game;
	LoggedUser m_user;
	GameManager * m_gameManager;
	RequestHandleFactory * m_handleFactory;
	virtual bool isRequestRelevant(request) override;
	virtual requestResult handleRequests(request) override;
	virtual void unexpected_logout() override;
	requestResult LeaveGame(request r);
	requestResult submitAnswer(request r);
	requestResult getQuestion(request r);
	requestResult getGameResults(request r);
private:
	clock_t begin_time;
};