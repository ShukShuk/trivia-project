#include "RoomAdminRequestHandler.h"
#include "Response.h"
#include "jsonPacketDeserializer.h"
std::vector<uint8_t> returnfinalbuf(std::vector<uint8_t> vec, int stamp);
RoomAdminRequestHandler::RoomAdminRequestHandler(Room * r, LoggedUser l)
{
	m_room = r;
	m_user = l;
}

bool RoomAdminRequestHandler::isRequestRelevant(request r)
{
	if (r.type >= 9 && r.type <= 11)
		return true;
	return false;
}

requestResult RoomAdminRequestHandler::handleRequests(request r)
{
	if(r.type == 9)//close game
	{
			
		return(closeRoom(r));
	}
	if (r.type == 10)//start game
	{
		return (startGame(r));
	}
	if (r.type == 11)
	{
		return(getRoomState(r));
	}
	return requestResult();
}
requestResult RoomAdminRequestHandler::startGame(request r)
{
	StartgameResponse sgr;
	sgr.status = 1;
	JsonResponsePacketSerializer jrps;
	m_room->startgame();
	std::vector<uint8_t> vec =  jrps.serializeResponse(sgr);
	requestResult rr;
	rr.response = NULL;
	rr.vec = returnfinalbuf(vec,10);
	rr.newHandler = m_handlerfactory->createGameRequestHandler(m_room,m_user);//for now
	return rr;
}
requestResult RoomAdminRequestHandler::getRoomState(request r)
{
	requestResult rr;
	JsonResponsePacketSerializer jrps;
	GetRoomStateResponse grsr;
	RoomData rd = m_room->getData();
	grsr.answerTimeout = rd.timePerQuestion;
	grsr.questionCount = rd.numberOfQuestions;
	grsr.status = 1;
	std::vector<LoggedUser> ls = m_room->getALLusers();//get players
	for (int i = 0; i < ls.size(); i++)
	{
		grsr.players.push_back(ls[i].getUserName());
	}
	grsr.hasGameBegun = false;
	std::vector<std::uint8_t> vec = jrps.serializeResponse(grsr);
	rr.newHandler = this;
	rr.response = NULL;
	rr.vec = returnfinalbuf(vec, 11);
	return rr;
}
std::vector<uint8_t> returnfinalbuf(std::vector<uint8_t> vec, int stamp)
{
	
	int len = vec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(stamp);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(vec[i]);
	}
	return buf;

}
requestResult  RoomAdminRequestHandler::closeRoom(request r)
{
	
	CloseRoomResponse crr;
	crr.status = true;
	requestResult rr;
	rr.newHandler = m_handlerfactory->createMenuRequestHandler(m_user.getUserName());
	JsonResponsePacketSerializer jrps;
	m_roommanager->deleteRoom(*m_room);
	std::vector<uint8_t> vec = jrps.serializeResponse(crr);
	rr.response = NULL;
	rr.vec = returnfinalbuf(vec, 9);
	return rr;
}

void RoomAdminRequestHandler::unexpected_logout()
{
	request r;
	r.buffer = (LPSTR)"";
	r.type = 999;
	requestResult rr = 	closeRoom(r);
	rr.newHandler->unexpected_logout();
}