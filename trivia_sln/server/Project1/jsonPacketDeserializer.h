#pragma once
#include <iostream>
#include <vector>
#include "json.hpp"
#include "RequestResult.h"
using json = nlohmann::json;
class JsonRequestPacketDeserializer
{
public:
	
	loginRequest deserializeLoginRequest(char *  buffer);

	SignupRequest deserializeSignupRequest(char* buffer);

	CreateRoomRequest deserializeCreateRoomRequest(char * buffer);

	JoinRoomRequest deserializeJoinRoomRequest(char * buffer);

	GetPlayersInRoomRequest deserializeGetPlayersRequest(char * buffer);

	QusetionDatafromWebSite deserializewebjson(char * buffer, int i);
	SubmitAnswerRequest deseializeSubmitRequest(char * buffer);

};