#pragma once
#include "sqlite3.h"
#include <string>
#include <vector>
#include <map>
#include <io.h>
#include <iostream>
#include "LoggedUser.h"
#include "Question.h"
#define dbFileName "bestDBever.sqlite"

class Idatabase
{
public:
	Idatabase();
	~Idatabase();

	bool doesUserExist(std::string str);
	std::map<int,LoggedUser> getHighscores();
	void pushScore(int score, std::string username);

	std::vector<Question> getQuestions(int num);


	sqlite3 *db;
private:
};
