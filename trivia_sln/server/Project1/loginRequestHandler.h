#pragma once
#include <iostream>
#include <string>
#include "loginManager.h"
#include "RequestHandleFactory.h"
#include "IRequestHandler.h"
#include "RequestResult.h"
#include "Helper.h"
#include "request.h"
class loginManager;
class RequestHandleFactory;
class c{};
class loginRequestHandler : public IRequestHandler
{
private:
	int type;
public:
	loginRequestHandler(loginManager *loginManag);
	requestResult handleRequests(request req);
	bool isRequestRelevant(request req);
	requestResult login(loginRequest req);
	requestResult signup(SignupRequest req);
	loginManager * m_loginManager;
	RequestHandleFactory * m_requestHandlerFactory;
	virtual void unexpected_logout() override;
};