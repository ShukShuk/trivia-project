#include "Game.h"
#include "Idatabase.h"

Game::Game(std::vector<Question> questions, std::vector<LoggedUser> players)
{
	m_questions = questions;
	for (int i = 0; i < players.size(); i++)
	{
		GameData gd{questions[0],0,0,0};
		m_players.insert(std::pair<LoggedUser, GameData>(players[i], gd));
	}
}

Question Game::getQuestionForUser(LoggedUser user)
{
	std::vector<std::string> v ;
	v.push_back("stope");
	Question * q = new Question("bla", "nope", v);
	return *q;
}

void Game::submitAnswer(LoggedUser user, int id)
{
	std::map<LoggedUser, GameData>::iterator it = m_players.begin();
	for (; it != m_players.end(); it++)
	{
		if (it->first.getUserName() == user.getUserName())
		{
			if (id == 3)
			{
				//correct answer
				it->second.correctAnswerCount += 1;
			}
			else
			{
				it->second.wrongAnswerCount += 1;
			}
			for (int i = 0; i < m_questions.size(); i++)
			{
				if(m_questions[i].getQuestion() == it->second.currentQuestion.getQuestion())
				{
					if(i +1 != m_questions.size())
						it->second.currentQuestion = m_questions[i + 1];
					break;
				}
			}

		}
	}
}
void Game::LeaveGame(LoggedUser l)
{
	std::map<const LoggedUser, GameData>::iterator it;
	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if (it->first.getUserName() == l.getUserName())
		{
			m_players.erase(it);
			break;
		}
	}
}
void Game::removePlayer(LoggedUser l)
{

	std::map<const LoggedUser, GameData>::iterator it;
	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if (it->first.getUserName() == l.getUserName())
		{
			PlayerResult temp;
			temp.averageAnswerTime = it->second.averangeAnswerTime;
			temp.correctAnswerCount = it->second.correctAnswerCount;
			temp.username = l.getUserName();
			temp.wrongAnswerCount = it->second.wrongAnswerCount;
			final_scores.push_back(temp);
			
			m_players.erase(it);
			break;
		}
	}
}
