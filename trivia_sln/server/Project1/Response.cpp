#include "Response.h"
#include "roomData.h"
std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
	json j;
	j["message"] = error.message;
	std::vector<uint8_t> after_cbor = json::to_cbor(j);

	return after_cbor;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(LoginResponse login)
{
	json j = {
		{"LoginStatus", login.LoginStatus}
	}; //the status for the error
	std::vector<uint8_t> after_cbor = json::to_cbor(j);
	return after_cbor;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(SignupResponse sign)
{
	
	json j;
	j["status"] = sign.SignupStatus; //the status for the error
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> myvec;
	for (int i = 0; i < ser.length(); i++)
	{
		myvec.push_back(bu[i]);
	}
	return myvec;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse create)
{
	json j;
	j["status"] = create.status; //the status for the error
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> myvec;
	for (int i = 0; i < ser.length(); i++)
	{
		myvec.push_back(bu[i]);
	}
	return myvec;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse join)
{
	json j;
	j["status"] = join.status;
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;

}
std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse player)
{
	
	auto jsonArray = json::array();
	for (auto i = 0; i < player.players.size(); i++)
	{
		json temp;
		temp["playerName"] = player.players[i];
		jsonArray.push_back(temp);
	}
	
	std::string der = jsonArray.dump();
	char * bu = (char*)der.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < der.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}


std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json j;
	auto jsonArray = json::array();
	j["status"] = response.status;
	for (int i = 0; i < response.rooms.size(); i++)
	{
		RoomData rd;
		rd = response.rooms[i];
		json temp;
		temp["id"] = rd.id;
		temp["name"] = rd.name;
		temp["maxplayers"] = rd.Maxplayer;
		temp["questionCount"] = rd.numberOfQuestions;
		temp["timePerQuestion"] =rd.timePerQuestion;
		temp["isactive"] = rd.isActive;
		temp["currplayers"] = rd.currplayesr;
		jsonArray.push_back(temp);
	}

	std::string ser = j.dump();
	std::string der = jsonArray.dump();
	std::string fer = ser + der;
	char * bu = (char*)fer.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < fer.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}



std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logout)
{
	json j;
	j["status"] = logout.status;
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(StartGameResponse start)
{
	json j;
	j["status"] = start.status; //the status for the error
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> myvec;
	for (int i = 0; i < ser.length(); i++)
	{
		myvec.push_back(bu[i]);
	}
	return myvec;

}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse roomstate)
{
	json j;
	j["ls"] = roomstate.players;
	j["answerTimeout"] = roomstate.answerTimeout;
	j["hasGameBegun"] = roomstate.hasGameBegun;
	j["questionCount"] = roomstate.questionCount;
	j["status"] = roomstate.status;
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}


std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse close)
{
	json j;
	j["status"] = close.status;
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}
std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse getq)
{
	json j;
	j["status"] = getq.status;
	j["question"] = getq.question;
	//j["answers"] = getq.answers;
	std::string ser = j.dump();
	ser = ser + "{space}";
	ser = ser + "{";
	for (std::map<unsigned int, std::string>::iterator it = getq.answers.begin(); it != getq.answers.end(); it++)
	{
		ser =  ser + (char)34 ;
		ser += std::to_string(it->first);
		ser += "\":\"";
		ser += it->second;
		ser += "\"" ;
		std::map<unsigned int, std::string>::iterator temp = it;
		temp++;
		if (temp != getq.answers.end())
		{
			ser = ser + ",";
		}
	}
	ser += "}";
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse sar)
{
	json j;
	j["status"] = sar.status;
	j["answerid"] = sar.correctAnswerid;
	std::string ser = j.dump();
	char * bu = (char*)ser.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < ser.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse result)
{
	json j;
	auto jsonArray = json::array();
	j["status"] = result.status;
	for (int i = 0; i < result.results.size(); i++)
	{
		json temp;
		PlayerResult tempp = result.results[i];
		temp["username"] =	tempp.username;
		temp["correctAnswerCount"] = tempp.correctAnswerCount;
		temp["wrongAnswerCount"] = tempp.wrongAnswerCount;
		temp["averageAnswerTime"] = tempp.averageAnswerTime;
		jsonArray.push_back(temp);
	}
	std::string ser = j.dump();
	std::string der = jsonArray.dump();
	std::string fer = ser + der;
	char * bu = (char*)fer.c_str();
	std::vector<std::uint8_t> vec;
	for (int i = 0; i < fer.length(); i++)
	{
		vec.push_back(bu[i]);
	}
	return vec;
}
