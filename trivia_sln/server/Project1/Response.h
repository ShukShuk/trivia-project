#pragma once
#include <iostream>
#include <vector>
#include "json.hpp"
#include "roomData.h"
#include <list>
#include "PlayerResultsH.h"
using json = nlohmann::json;
typedef struct LoginResponse
{
	unsigned int LoginStatus;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int SignupStatus;
}SignupResponse;

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;

typedef struct LogoutResponse
{
	unsigned int status;
};

typedef struct JoinRoomResponse
{
	unsigned int status;

};

typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;

};

typedef struct HighscoreResponse
{


};

typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;


};

typedef struct CreateRoomResponse
{
	unsigned int status;
	
};
typedef struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;


}GetRoomStateResponse;
typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;
typedef struct StartGameResponse
{
	unsigned int status;
}StartgameResponse;
typedef struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
}GetQuestionResponse;
typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerid;
}SubmitAnswerResponse;

typedef struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResult> results;
}GetGameResultsResponse;


class JsonResponsePacketSerializer
{
public:
	std::vector<uint8_t> serializeResponse(ErrorResponse error);
	std::vector<uint8_t> serializeResponse(LoginResponse login);
	std::vector<uint8_t> serializeResponse(SignupResponse sign);
	std::vector<uint8_t> serializeResponse(CreateRoomResponse create);
	std::vector<uint8_t> serializeResponse(JoinRoomResponse join);
	std::vector<uint8_t> serializeResponse(GetPlayersInRoomResponse player);
	std::vector<uint8_t> serializeResponse(GetRoomsResponse join);
	std::vector<uint8_t> serializeResponse(LogoutResponse logout);
	std::vector<uint8_t> serializeResponse(StartGameResponse start);


	std::vector<uint8_t> serializeResponse(GetRoomStateResponse roomstate);
	std::vector<uint8_t> serializeResponse(CloseRoomResponse close);
	std::vector<uint8_t> serializeResponse(GetQuestionResponse getq);
	std::vector<uint8_t> serializeResponse(SubmitAnswerResponse sar);
	std::vector<uint8_t> serializeResponse(GetGameResultsResponse result);

};
