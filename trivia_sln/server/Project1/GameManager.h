#pragma once
#include "Idatabase.h"
#include "Game.h"
#include "Room.h"
#include "roomManager.h"

class GameManager
{
	
public:
	GameManager(Idatabase * db);
	std::vector<Game*> m_games;
	Game * createGame(Room r);
	void deleteGameifQUIT(Game g, LoggedUser l, roomManager * rm);
	void deleteGame(Game g, LoggedUser l, roomManager * rm);
	//void deleteGame(Game g, LoggedUser l);

	Idatabase * m_database;
};