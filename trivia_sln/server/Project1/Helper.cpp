#include "Helper.h"
#include <vector>
#include <math.h>
int helper::gettypeFromVECTOR(std::vector<std::uint8_t> a)
{
	return (int)a[0];
}
int helper::getsizeFrom(std::vector<std::uint8_t>a)
{
	int ans = 0;
	for (int i = 0; i < 4; i++)
	{
		ans += (int)a[i] * pow(256, 3 -i);
	}
	return ans;
}
std::vector<std::uint8_t> helper::getMessageTypeCode(SOCKET sc)
{
	char * ans = getPartFromSocket(sc, 1, 0);
	std::vector<std::uint8_t> f;
	f.push_back((std::uint8_t)ans[0]);
	return f;

}
std::vector<std::uint8_t> helper::GetSize(SOCKET sc)
{
	char * ans = getPartFromSocket(sc, 4, 0);
	std::vector<std::uint8_t> f;
	for (int i = 0; i < 4; i++)
	{
		f.push_back((std::uint8_t)ans[i]);
	}
	return f;
}
char * helper::getData(SOCKET sc, int size)
{
	char * ans = getPartFromSocket(sc, size , 0);
	
	return ans;
}

char * helper::getPartFromSocket(SOCKET sc, int bytesNum,int flags)
{
	if (bytesNum == 0)
	{

		return (LPSTR)"";
	}
	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		return (LPSTR)"";
	}

	data[bytesNum] = 0;
	return data;
	return nullptr;
}
void helper::sendData(SOCKET sc, std::uint8_t * vec,int size )
{
	

	if (send(sc, (const char *)vec, size, 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}
void helper::sendData(SOCKET sock ,std::string  msg)
{
	if (send(sock,msg.c_str(), msg.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}