#pragma once
#include "Idatabase.h"
#include "LoggedUser.h"
#include <map>
class Highscore {
public:
	Highscore(Idatabase * db);
	std::map<int, LoggedUser> getHighscores();
private:
	Idatabase * m_database;
	 
 };