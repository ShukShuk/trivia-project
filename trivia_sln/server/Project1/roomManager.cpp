#include "roomManager.h"

Room * roomManager::createRoom(LoggedUser l, RoomData & r)
{
	r.id = idforrooms;
	r.isActive = false;
	r.currplayesr = 0;
	Room * ro = new Room(r);
	ro->addUser(l);
	std::pair<int, Room*> a(idforrooms, ro) ;
	m_rooms.insert(a);
	idforrooms++;
	return ro;
}

void roomManager::deleteRoom(RoomData rd)
{

	std::map<int, Room *>::iterator it = m_rooms.begin();
	for (; it != m_rooms.end(); it++)
	{
		Room * temp = (it->second);
		RoomData rdtemp = temp->getData();
		if (rd.id == rdtemp.id)
		{
			m_rooms.erase(it);
			break;
		}
	}
}

void roomManager::deleteRoom(Room r)
{
	std::map<int, Room*>::iterator it = m_rooms.begin();
	for (; it != m_rooms.end(); it++)
	{
		Room * temp = (it->second);
		RoomData rd = temp->getData();
		if (rd.id == r.getData().id)
		{
			m_rooms.erase(it);
			break;
		}
	}
}

unsigned int roomManager::getRoomState(int ID)
{
	for (std::map<int, Room*>::iterator it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		if (it->second->getData().id == ID)
		{
			return it->second->getData().isActive;
		}
	}
	return 0;
}

std::vector<RoomData> roomManager::getRooms()
{
	std::vector<RoomData> dataarr;
	for (std::map<int, Room*>::iterator it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		dataarr.push_back(it->second->getData());
	}
	return dataarr;
}
Room * roomManager::getRoombyid(int id)
{
	for (std::map<int, Room*>::iterator it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		if (id == it->first)
		{
			return (it->second);
		}
	
	}
	return NULL;
}
Room * roomManager::getRoombyusername(LoggedUser usernamepar)
{
	for (std::map<int, Room*>::iterator it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		std::vector<LoggedUser> vec =  it->second->getALLusers();
		for (int i = 0; i < vec.size(); i++)
		{
			if (vec[i].getUserName() == usernamepar.getUserName())
			{
				return(it->second);
			}
		}

	}
	return NULL;
}



