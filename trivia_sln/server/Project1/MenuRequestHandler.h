#pragma once
#include "LoggedUser.h"
#include <iostream>
#include "roomManager.h"
#include "HighScore.h"
#include "RequestHandleFactory.h"
class RequestHandleFactory;
class HighScore;
class roomManager;
class d {};
class MenuRequestHandler : public IRequestHandler
{
private: 

public:
	Highscore * m_highscoreTable;
	RequestHandleFactory * m_handlefactory;
	MenuRequestHandler(std::string username, roomManager* rm);
	virtual bool isRequestRelevant(request);
	virtual requestResult handleRequests(request);
	requestResult signout(request r);
	requestResult GetPlayersInRoom(request r);
	requestResult joinRoom(request r);
	requestResult createRoom(request r);
	requestResult getRooms(request r);
	roomManager *m_roomManager;
	LoggedUser m_user;
	virtual void unexpected_logout() override;
	

};