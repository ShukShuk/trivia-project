#pragma once
#include <iostream>
#include "sqlite3.h"
#include "communicator.h"
#include "Idatabase.h"
#include "RequestHandleFactory.h"
#include "WSAInitializer.h"
#define PORT 274
class Server
{
public:
	Server();
	void run();
	

private:
	Idatabase db;
	RequestHandleFactory m_RequestHndlFactory;
	Communicator m_communicator;
	
};