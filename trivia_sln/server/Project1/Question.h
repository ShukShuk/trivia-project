#pragma once
#include <string>
#include <vector>


class Question
{
public:
	Question(std::string question, std::string correctAns, std::vector<std::string> possibleAnswers);
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAnswer();

private:
	std::string m_question;
	std::string m_correctAnswer;
	std::vector<std::string> m_possibleAnswers;

};
