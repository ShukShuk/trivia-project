#pragma once
#include "Room.h"
#include "LoggedUser.h"
#include "roomData.h"
#include <map>
class roomManager
{
private : 
	std::map<int, Room*> m_rooms;
	int idforrooms =0;
public:	
	Room *  createRoom(LoggedUser, RoomData &);
	void deleteRoom(RoomData);
	void deleteRoom(Room);
	unsigned int getRoomState(int ID);
	std::vector <RoomData>  getRooms();
	Room * getRoombyid(int id);

	Room * getRoombyusername(LoggedUser usernamepar);

};