#pragma once
#include <iostream>
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "roomManager.h"
#include "RequestHandleFactory.h"

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(Room * r, LoggedUser l);
	virtual bool isRequestRelevant(request) override;
	virtual requestResult handleRequests(request) override;

	requestResult startGame(request r);

	requestResult getRoomState(request);

	requestResult closeRoom(request r);

	Room * m_room;
	LoggedUser m_user;
	roomManager * m_roommanager;
	RequestHandleFactory * m_handlerfactory;
	virtual void unexpected_logout() override;
};

