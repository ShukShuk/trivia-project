#include "Room.h"

Room::Room()
{
}

Room::Room(RoomData rd)
{
	m_data = rd;
}

void Room::addUser(LoggedUser l)
{
	m_users.push_back(l);
	m_data.currplayesr += 1;
}

void Room::removeUser(LoggedUser l)
{
	auto search = m_users.begin();
	for (int i =0;i<m_users.size();i++)
	{
		if (m_users[i].getUserName() == l.getUserName())
		{
			m_users.erase(search);
			m_data.currplayesr--;
			break;
			
		}
		search++;
	}
}

std::vector<LoggedUser> Room::getALLusers()
{
	return m_users;
}

RoomData Room::getData()
{
	return m_data;
}

void Room::startgame()
{
	m_data.isActive = true;
}
