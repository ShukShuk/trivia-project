#pragma once
#include <iostream>
#include <string>
#include "loginManager.h"
#include "RequestResult.h"
#include "loginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoggedUser.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
class GameRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class loginRequestHandler;
class MenuRequestHandler;
class RequestHandleFactory
{
public:
	RequestHandleFactory(Idatabase* db);
 loginRequestHandler * createLoginRequestHndler();
 MenuRequestHandler * createMenuRequestHandler(std::string username);
 RoomAdminRequestHandler * createRoomAdminRequestHandler(LoggedUser l, Room *r);
 RoomMemberRequestHandler * createRoomMemberRequestHandler(LoggedUser l, Room *r);
 GameRequestHandler * createGameRequestHandler(Room * r, LoggedUser l);
 roomManager m_roomManager;
private:
	loginManager m_loginManager;
	Highscore * m_highscore;
	GameManager m_gameManager;
};