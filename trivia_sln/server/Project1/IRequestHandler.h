#pragma once
#include<iostream>
#include "request.h"
#include "typederes.h"
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(request) = 0;
	virtual requestResult handleRequests(request) = 0;
	virtual void unexpected_logout() = 0;
};