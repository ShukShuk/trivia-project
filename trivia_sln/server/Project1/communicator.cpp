#pragma comment (lib, "ws2_32.lib")

#include "communicator.h"
#define PORT 6743
#include <thread>
#include "Helper.h"
#include "jsonPacketDeserializer.h"
#include "Response.h"
#include "typederes.h"
void Communicator::startNewThreadForNewClient()
{
}

Communicator::Communicator(RequestHandleFactory reqHndl) : m_requesthandlerfactory(reqHndl)
{
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}
void Communicator::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread a(&Communicator::handleRequests, this, client_socket);//remember to add the last parameter
	a.detach(); 
}


void Communicator::handleRequests(SOCKET sock)
{
	std::string username = "";
	helper h;
	loginRequestHandler l = *(m_requesthandlerfactory.createLoginRequestHndler());
	l.m_requestHandlerFactory = &m_requesthandlerfactory;
	m_clients.insert(std::pair<SOCKET, IRequestHandler*>(sock, &l));
	auto search = m_clients.find(sock);
	while (true)
	{
		std::vector<std::uint8_t> type;
		type= h.getMessageTypeCode(sock);
		std::vector<std::uint8_t> size;
		size = h.GetSize(sock);
		char * data;
		//std::uint8_t sizea[4] = { size[0],size[1],size[2],size[3] };
		int isize = h.getsizeFrom(size);
		int itype = h.gettypeFromVECTOR(type);
	
		try
		{
			data = h.getData(sock, isize);
		}
		catch (const std::exception&)
		{
			IRequestHandler * i = search->second;
			i->unexpected_logout();
			m_clients.erase(search);
			break;
		};		
		request r;
		r.type = itype;
		r.buffer = data;
		if (r.type == 1)
		{
			JsonRequestPacketDeserializer ds;
			username = ds.deserializeLoginRequest(data).username;
		}
		else if (r.type == 2)
		{
			JsonRequestPacketDeserializer ds;
			username = ds.deserializeSignupRequest(data).username;
		}
		IRequestHandler * i = search->second;
		//gmail
		requestResult rr;
		if ((*i).isRequestRelevant(r))
		{
			try
			{
				rr = (*i).handleRequests(r);
			}
			catch(const std::exception&)
			{
				
			}
			if(r.type == 1)//login
				h.sendData(sock, std::string(rr.response));
			else if(r.type == 80 || r.type == 15)//leave game or request password
			{
				//pass
			}
			else
			{
				std::vector<std::uint8_t>  v = rr.vec;
				int sizea = v.size();
				std::uint8_t * ar = new std::uint8_t[sizea];
				for (int i = 0; i < sizea; i++)
				{
					ar[i] = v[i];
				}
				h.sendData(sock,ar,sizea);
			}
			search->second = rr.newHandler;

		}
		else
		{/*
			JsonResponsePacketSerializer jrps;
			ErrorResponse er;
			er.message = "error, request isn't relevant";
			std::vector<std::uint8_t> jsonvec = jrps.serializeResponse(er);
			int vecsize = jsonvec.size();
			std::uint32_t uvexsize = (std::uint32_t)vecsize;
			std::uint8_t sizearr[4] = { uvexsize };
			memcpy((std::uint8_t *)sizearr, (std::uint8_t*)&vecsize, sizeof(int));
			std::vector<std::uint8_t> finalvec;
			finalvec.push_back((std::uint8_t)99);
			for (int i = 3; i >= 0; i++)
			{
				finalvec.push_back(sizearr[i]);
			}
			for (int i = jsonvec.size() - 1; i >= 0; i++)
			{
				finalvec.push_back(jsonvec[i]);
			}
			std::uint8_t * farr = &finalvec[0];
			h.sendData(sock, farr, finalvec.size());*/
		}
	}
}

