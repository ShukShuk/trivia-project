#pragma once
#include "roomData.h"
#include "LoggedUser.h"
#include <vector>
class Room 
{
public:
	Room();
	Room(RoomData rd);
	void addUser(LoggedUser l);
	void removeUser(LoggedUser l);
	std::vector<LoggedUser> getALLusers();
	RoomData getData();
	void startgame();
private :
	RoomData m_data;
	std::vector <LoggedUser> m_users;
	
};