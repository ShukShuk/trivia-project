#include "loginRequestHandler.h"
#include "Helper.h"
#include "jsonPacketDeserializer.h"
#include "MenuRequestHandler.h"
#include "Response.h"
#include "typederes.h";
loginRequestHandler::loginRequestHandler(loginManager* loginManag) : m_loginManager(loginManag)
{
}
void set_size(char *a,int size)
{
	for (int i = 4; i >0; i--)
	{
		a[i] = (size / pow(256,4-i) + 48);
	}

}
void add_vec(std::vector<std::uint8_t> a, char *b)
{
	int j = 0;
	for (int i = 5; i < a.size() + 5; i++)
	{
		b[i] = a[j];
		j++;
	}
}
requestResult loginRequestHandler::handleRequests(request req)
{
	loginRequest lr;
	SignupRequest sr;
	LoginResponse loginr;
	SignupResponse signr;
	JsonRequestPacketDeserializer j;
	JsonResponsePacketSerializer js;
	requestResult rr;
	
	std::vector<std::uint8_t> vec;
	//deserializer
	bool suc;
	if (req.type == 1)//login
	{
	
		lr = j.deserializeLoginRequest(req.buffer);
		suc = (*m_loginManager).login(lr.username, lr.password);
		if (suc)
		{
			
			rr.newHandler = (m_requestHandlerFactory->createMenuRequestHandler(lr.username));//for now
			loginr.LoginStatus = true;
		}
		else
		{
			rr.newHandler = this;
			loginr.LoginStatus = false;
		}
		vec = js.serializeResponse(loginr);
		char * res = new char[vec.size() + 6];
		res[vec.size() + 5] = NULL;
		for (int i = 0; i < vec.size() +5;i++)
		{
			res[i] = '1';
		}
		set_size(res, vec.size());
		add_vec(vec, res);
		rr.response = res;
		
		
		return rr;
	}
	else if (req.type == 2)
	{
		
		sr = j.deserializeSignupRequest(req.buffer);
		if ((*m_loginManager).signup(sr.username, sr.password, sr.email))
		{
			
			MenuRequestHandler* m = m_requestHandlerFactory->createMenuRequestHandler(sr.username);

			rr.newHandler = m;//for now
			signr.SignupStatus = true;
			vec = js.serializeResponse(signr);
			
		}
		else
		{
			rr.newHandler = this;
			signr.SignupStatus = false;
			vec = js.serializeResponse(signr);
		}
		int len = vec.size();
		std::vector<std::uint8_t> buf;
		buf.push_back(2);
		for (int i = 1; i <= 4; i++)
		{
			buf.push_back(len / pow(256, 4 - i));
		}
		for (int i = 0; i < len; i++)
		{
			buf.push_back(vec[i]);
		}
		rr.response = NULL;
		rr.vec = buf;
		return rr;
	}
	else if(req.type == 80)//gmail
	{
		helper h;
		(*m_loginManager).findpass(req.buffer);
		
		rr.newHandler = this;
		return rr;
	}
	
}

bool loginRequestHandler::isRequestRelevant(request req)
{
	if (req.type == 1 || req.type == 2 || req.type == 80)
	{
		return true;
	}
	else
	{
		return false;
	}
}

requestResult loginRequestHandler::login(loginRequest req)
{
	return requestResult();
}

requestResult loginRequestHandler::signup(SignupRequest req)
{
	return requestResult();
}

void loginRequestHandler::unexpected_logout()
{

}
