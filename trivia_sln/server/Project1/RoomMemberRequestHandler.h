#pragma once
#include "IRequestHandler.h"
#include <iostream>
#include "Room.h"
#include "LoggedUser.h"
#include "roomManager.h"
#include "RequestHandleFactory.h"
class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(Room *r, LoggedUser);
	virtual bool isRequestRelevant(request) override;
	virtual requestResult handleRequests(request) override;
	requestResult getRoomState(request r);
	requestResult leaveRoom(request r);
	Room * m_room;
	LoggedUser m_user;
	roomManager * m_roommanager;
	RequestHandleFactory * m_handlerfactory;
	virtual void unexpected_logout() override;

};
