#include "HighScore.h"

Highscore::Highscore(Idatabase * db)
{
	this->m_database = db;
}

std::map<int , LoggedUser> Highscore::getHighscores()
{
	return(m_database->getHighscores());
}
