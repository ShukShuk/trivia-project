#include "MenuRequestHandler.h"
#include "Response.h"
#include "jsonPacketDeserializer.h"
MenuRequestHandler::MenuRequestHandler(std::string username, roomManager* rm)
{
	m_user = *(new LoggedUser(username));
	m_roomManager = rm;

}

bool MenuRequestHandler::isRequestRelevant(request r)
{
	if (r.type >= 3 && r.type <= 8)
		return true;
	return false;
}

requestResult MenuRequestHandler::handleRequests(request r )
{
	JsonRequestPacketDeserializer jrpd;
	CreateRoomRequest crr;
	if (r.type == 3)//createroom stamp
	{
		return(createRoom(r));//dont forget to save the ans
	
	}
	if (r.type == 4)//get rooms
	{
		return(getRooms(r));
	}
	if (r.type == 5)//joinstamp
	{
		return(joinRoom(r));
	}
	if (r.type == 6)
	{
		return(GetPlayersInRoom(r));
	}
	if (r.type == 7)
	{
		return(signout(r));
	}
	if (r.type == 8)
	{
		//get highscores
	}
}
requestResult  MenuRequestHandler::signout(request r)
{
	LogoutResponse lr;
	requestResult rr;
	loginRequestHandler * lrh = (m_handlefactory->createLoginRequestHndler());
	lr.status = lrh->m_loginManager->logout(m_user.getUserName());
	JsonResponsePacketSerializer jrps;
	std::vector<std::uint8_t> bufvec = jrps.serializeResponse(lr);
	int len = bufvec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(7);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(bufvec[i]);
	}
	rr.response = NULL;
	rr.vec = buf;
	rr.newHandler = lrh;//now;
	return rr;


}
requestResult MenuRequestHandler::GetPlayersInRoom(request r)
{
	requestResult rr;
	JsonRequestPacketDeserializer jrpd;
	JsonResponsePacketSerializer jrps;
	GetPlayersInRoomRequest gpirr;
	GetPlayersInRoomResponse gpirresp;
	gpirr = jrpd.deserializeGetPlayersRequest(r.buffer);
	std::vector<LoggedUser> vec = m_roomManager->getRoombyid(gpirr.roomid)->getALLusers();
	std::vector<std::string> finvec;
	for (int i = 0; i < vec.size(); i++)
	{
		finvec.push_back(vec[i].getUserName());
	}
	gpirresp.players = finvec;
	std::vector<std::uint8_t> bufvec = jrps.serializeResponse(gpirresp);
	int len = bufvec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(6);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(bufvec[i]);
	}
	rr.response = NULL;
	rr.vec = buf;
	Room * roomfors = m_roomManager->getRoombyid(gpirr.roomid);
	rr.newHandler = m_handlefactory->createRoomMemberRequestHandler(m_user, roomfors);//now;
	return rr;
}

requestResult  MenuRequestHandler::joinRoom(request r)
{
	requestResult rr;
	JsonRequestPacketDeserializer jrpd;
	JsonResponsePacketSerializer jrps;
	JoinRoomRequest jrr;
	JoinRoomResponse jrresponse;
	jrr = jrpd.deserializeJoinRoomRequest(r.buffer);
	Room * ro = m_roomManager->getRoombyid(jrr.roomid);
	std::vector<LoggedUser> users = ro->getALLusers();
	bool flag = false;
	for (int i = 0; i < users.size(); i++)
	{
		if (users[i].getUserName() == m_user.getUserName())
		{
			jrresponse.status = 0;
			flag = true;
		}
	}
	if (!flag)
	{
		jrresponse.status = 1;
		ro->addUser(m_user);
	}
	std::vector<std::uint8_t> vec = jrps.serializeResponse(jrresponse);
	int len = vec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(5);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(vec[i]);
	}
	rr.response = NULL;
	rr.vec = buf;
	rr.newHandler = this;//now;
	return rr;


}
requestResult MenuRequestHandler::createRoom(request r)
{
	JsonRequestPacketDeserializer jrpd;
	CreateRoomRequest crr;
	crr = jrpd.deserializeCreateRoomRequest(r.buffer);
	RoomData rd;
	rd.Maxplayer = crr.maxUsers;
	rd.name = crr.roomName;
	rd.numberOfQuestions = crr.questionCount;
	rd.timePerQuestion = crr.answerTimeout;
	Room * roomforc =  m_roomManager->createRoom(this->m_user, rd);
	requestResult rr;
	JsonResponsePacketSerializer jrps; 
	CreateRoomResponse crres;
	crres.status = true;
	std::vector<std::uint8_t> vec = jrps.serializeResponse(crres);
	int len = vec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(3);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(vec[i]);
	}
	rr.response = NULL;
	rr.vec = buf;
	rr.newHandler = m_handlefactory->createRoomAdminRequestHandler(m_user,roomforc);//now;
	return rr;

}

requestResult MenuRequestHandler::getRooms(request r)
{
	requestResult rr;
	JsonResponsePacketSerializer jrps;
	GetRoomsResponse jrr;
	 jrr.rooms= this->m_roomManager->getRooms();
	 if (jrr.rooms.size() == 0)
		 jrr.status = 0;
	 else
	 {
		 jrr.status = 1;
	 }
	 std::vector<std::uint8_t>vec =jrps.serializeResponse(jrr);
	 int len = vec.size();
	 std::vector<std::uint8_t> buf;
	 buf.push_back(4);
	 for (int i = 1; i <= 4; i++)
	 {
		 buf.push_back(len / pow(256, 4 - i));
	 }
	 for (int i = 0; i < len; i++)
	 {
		 buf.push_back(vec[i]);
	 }
	 rr.response = NULL;
	 rr.vec = buf;
	 rr.newHandler = this;//now;
	 return rr;
}

void MenuRequestHandler::unexpected_logout()
{
	request r;
	r.buffer = (LPSTR)"";
	r.type = 999;
	signout(r);
}
