#pragma once
#include <iostream>
#include "Question.h"
#include <map>
#include "LoggedUser.h"
#include "GameData.h"
#include "Idatabase.h"
#include <unordered_map>
#include "Response.h"
struct cmpByString {
	bool operator()(const LoggedUser & a, const LoggedUser & b) const {
		std::string ab = a.getUserName();
		std::string bc = b.getUserName();
		return ab[0] < bc[0];
	}
};
class Game
{
public:
	
	Game(std::vector<Question> qusetions, std::vector<LoggedUser> players);
	Question getQuestionForUser(LoggedUser user);
	void submitAnswer(LoggedUser user, int id);
	void LeaveGame(LoggedUser l);
	void removePlayer(LoggedUser l);
	std::map<LoggedUser, GameData,cmpByString> m_players;
	std::vector < PlayerResult> final_scores;
private:
	std::vector<Question> m_questions;
	
};
