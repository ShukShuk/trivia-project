#pragma once
#include <iostream>
typedef struct PlayerResult
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
}PlayerResult;