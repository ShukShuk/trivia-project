#include "RoomMemberRequestHandler.h"
#include "jsonPacketDeserializer.h"
#include "Response.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room * r, LoggedUser l)
{
	m_room = r;
	m_user = l;
	
}
bool RoomMemberRequestHandler::isRequestRelevant(request r)
{
	if(r.type == 9 || r.type == 11)
		return true;
	return false;
}
requestResult RoomMemberRequestHandler::handleRequests(request r )
{
	if (r.type == 9)//leave game
	{
		return(leaveRoom(r));
	}
	if (r.type == 11)//get room state
	{
		return(getRoomState(r));
	}
	
}
std::vector<uint8_t> returnfinalbuf2(std::vector<uint8_t> vec, int stamp)
{

	int len = vec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(stamp);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(vec[i]);
	}
	return buf;
}
requestResult RoomMemberRequestHandler::getRoomState(request r)
{
	JsonResponsePacketSerializer jrps;
	GetRoomStateResponse grsr;
	Room * roomofid = m_roommanager->getRoombyid(m_room->getData().id);
	if (roomofid == NULL)
	{
		grsr.status = 0;
		grsr.hasGameBegun = 0;
		grsr.questionCount = 0;
		grsr.answerTimeout = 0;
		std::vector<std::string> ls;
		grsr.players = ls;
		
	}
	else
	{
		grsr.status = true;
		RoomData rd = m_room->getData();
		grsr.answerTimeout = rd.timePerQuestion;
		grsr.questionCount = rd.numberOfQuestions;
		std::vector<LoggedUser> ls = m_room->getALLusers();
		for(int i =0;i<ls.size();i++)
		{
			grsr.players.push_back(ls[i].getUserName());
		}
		grsr.hasGameBegun = rd.isActive;
	}
	std::vector<uint8_t> vec =  jrps.serializeResponse(grsr);
	requestResult rr;
	rr.vec = returnfinalbuf2(vec,11);
	rr.response = NULL;
	if (grsr.hasGameBegun)
	{
		rr.newHandler =  m_handlerfactory->createGameRequestHandler(m_room,m_user);//it will switch to the game handler
	}
	else
	{
		rr.newHandler = this;
	}

	return rr;
}
requestResult RoomMemberRequestHandler::leaveRoom(request r)
{
	CloseRoomResponse crr;
	crr.status = true;
	requestResult rr;
	rr.newHandler = m_handlerfactory->createMenuRequestHandler(m_user.getUserName());
	JsonResponsePacketSerializer jrps;
	m_room->removeUser(m_user);
	std::vector<uint8_t> vec = jrps.serializeResponse(crr);
	rr.response = NULL;
	rr.vec = returnfinalbuf2(vec, 9);
	return rr;
}

void RoomMemberRequestHandler::unexpected_logout()
{
	request r;

	r.buffer =(LPSTR)"";
	r.type = 999;
	requestResult rr = leaveRoom(r);
	rr.newHandler->unexpected_logout();
}
