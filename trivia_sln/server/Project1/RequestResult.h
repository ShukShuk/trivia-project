#pragma once
#include <iostream>
#include "IRequestHandler.h"
#include <vector>
#include <cstdint>


typedef struct Request
{
	std::string username;
	std::string password;
}loginRequest;
typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;
typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;
typedef struct JoinRoomRequest
{
	unsigned int roomid;
} JoinRoomRequest;
typedef struct GetPlayersInRoomRequest
{
	unsigned int roomid;
}GetPlayersInRoomRequest;
typedef struct QusetionDatafromWebSite
{
	std::string category;
	std::string type;
	std::string difficulty;
	std::string question;
	std::string	correct_answer;
	std::vector<std::string> incorrect_answers;
}QusetionDatafromWebSite;
typedef struct webjson
{
	int response_code;
	std::vector<QusetionDatafromWebSite> results;
}webjson;
typedef struct QuestionfromDataBase
{
	std::string question;
	std::string	correct_answer;
	std::vector<std::string> incorrect_answers;
};
typedef struct SubmitAnswerRequest
{
	unsigned int answerid;
}SubmitAnswerRequest;