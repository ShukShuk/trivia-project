#include "jsonPacketDeserializer.h"
#include <bitset>
#include <sstream>
#include <string>
#include "roomData.h"
#include <Windows.h>
std::string convertfrombin(char * a)
{
	std::string finalstr;
	std::string line(a);
	std::istringstream in(line);
	std::bitset<8> bs;
	while (in >> bs)
	{
		finalstr += char(bs.to_ulong());
	}
	return finalstr;
}
loginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(char *  buffer)
{
	std::string fst = convertfrombin(buffer);
	json j = json::parse(fst);
	loginRequest temp{ j["username"], j["password"] };
	return temp;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(char * buffer)
{
//	
	json j = json::parse(buffer);
	SignupRequest temp{ j["username"], j["password"], j["email"] };
	return temp;
}
CreateRoomRequest JsonRequestPacketDeserializer:: deserializeCreateRoomRequest(char * buffer)
 {
	std::cout << buffer << std::endl;
	json j = json::parse(buffer);
	CreateRoomRequest temp{ j["roomName"],j["maxUsers"], j["questionCount"], j["answerTimeout"] };
	return temp;
 }
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(char * buffer)
{
	json j = json::parse(buffer);
	JoinRoomRequest temp{ j["roomid"] };
	return temp;
}
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(char * buffer)
{
	json j = json::parse(buffer);
	GetPlayersInRoomRequest temp{ j["roomid"] };
	return temp;
}

QusetionDatafromWebSite JsonRequestPacketDeserializer::deserializewebjson(char * buffer, int i)
{
	
	try
	{
		if (buffer[std::string(buffer).size() - 1] == ']')
		{
			buffer[std::string(buffer).size() - 1] = NULL;
		}
		
		 json j = json::parse(buffer);
		 QusetionDatafromWebSite temp{ j["category"], j["type"], j["difficulty"], j["question"], j["correct_answer"], j["incorrect_answers"] };
		 return temp;
	}
	catch (std::exception exp)
	{
		std::cout << buffer << std::endl;
		std::cout << i << std::endl;
		QusetionDatafromWebSite temp2;
		temp2.type = "error";
		return temp2;
	}


	
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deseializeSubmitRequest(char * buffer)
{
	json j = json::parse(buffer);
	SubmitAnswerRequest sar{j["answerid"]};
	return sar;
}





