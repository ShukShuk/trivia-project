#pragma once
#include <iostream>
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include "IRequestHandler.h"
#include "RequestHandleFactory.h"
#include "RequestResult.h"
#include "request.h"
class RequestHandleFactory;
class Communicator
{
private:
	std::map<SOCKET,IRequestHandler*> m_clients;
	RequestHandleFactory m_requesthandlerfactory;
	void startNewThreadForNewClient();
	SOCKET _socket;
public:
	Communicator(RequestHandleFactory reqHndl);
	void bindAndListen();
	void accept();
	void handleRequests(SOCKET client_socket);
	

};

