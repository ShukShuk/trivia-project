#include "GameRequestHandler.h"
#include "Response.h"
#include "jsonPacketDeserializer.h"
#define CORRECT_SOUND_TIME 1.8
#define WRONG_SOUND_TIME 2.1
std::vector<uint8_t> returnfinalbu3(std::vector<uint8_t> vec, int stamp)
{

	int len = vec.size();
	std::vector<std::uint8_t> buf;
	buf.push_back(stamp);
	for (int i = 1; i <= 4; i++)
	{
		buf.push_back(len / pow(256, 4 - i));
	}
	for (int i = 0; i < len; i++)
	{
		buf.push_back(vec[i]);
	}
	return buf;
}
GameRequestHandler::GameRequestHandler(LoggedUser user, GameManager * gManager,Room * r,RequestHandleFactory *rhf)
{
	m_user = user;
	m_gameManager = gManager;
	m_handleFactory = rhf;
	bool flag = false;
	for (int i = 0; i < m_gameManager->m_games.size();i++ )
	{
		std::map<LoggedUser, GameData,cmpByString> tempmap = m_gameManager->m_games[i]->m_players;
		std::map<LoggedUser, GameData>::iterator mapit;
		for (mapit = tempmap.begin(); mapit != tempmap.end(); mapit++)
		{
			LoggedUser temp = mapit->first;
			if (temp.getUserName() == user.getUserName())
			{
				m_game = m_gameManager->m_games[i];
				flag = true;
				break;
			}
		}
	}
	if (!flag)
	{
		this->m_game = 	m_gameManager->createGame(*r);
	}
	begin_time = clock();
}

bool GameRequestHandler::isRequestRelevant(request)
{
	return true;
}

requestResult GameRequestHandler::handleRequests(request r)
{
	if (r.type == 12)//get quwstion request
	{
		return(getQuestion(r));
	}
	if (r.type == 13)
	{
		return(submitAnswer(r));
	}
	if (r.type == 14)
	{
		return(getGameResults(r));
	}
	if (r.type == 15)
	{
		return(LeaveGame(r));
	}
	return requestResult();
}

requestResult GameRequestHandler::LeaveGame(request r)
{
	if (m_game->m_players.size() == 1)
	{
		m_gameManager->deleteGameifQUIT(*m_game, m_user, &(m_handleFactory->m_roomManager));
	}
		m_game->LeaveGame(m_user);
	
	requestResult rr;
	rr.newHandler = m_handleFactory->createMenuRequestHandler(m_user.getUserName());
	rr.response = NULL;
	rr.vec.push_back(10);
	return rr;
}
requestResult GameRequestHandler::submitAnswer(request r)
{
	
	JsonRequestPacketDeserializer jrpd;
	JsonResponsePacketSerializer jrps;
	SubmitAnswerRequest sar = jrpd.deseializeSubmitRequest(r.buffer);
	SubmitAnswerResponse saresp;
	m_game->submitAnswer(m_user, sar.answerid);
	saresp.status = 1;
	saresp.correctAnswerid = 3;
	std::vector<uint8_t>vec = jrps.serializeResponse(saresp);
	requestResult rr;
	rr.newHandler = this;
	rr.response = NULL;
	rr.vec = returnfinalbu3(vec, 13);
	return rr;
}
requestResult GameRequestHandler::getQuestion(request r)
{
	JsonResponsePacketSerializer jrps;
	GetQuestionResponse gqr;
	std::map<unsigned int, std::string> mapans;

	std::map<LoggedUser, GameData>::iterator it = m_game->m_players.begin();
	for (; it != m_game->m_players.end(); it++)
	{
		if (it->first.getUserName() == m_user.getUserName())
		{
			gqr.question = it->second.currentQuestion.getQuestion();
			std::vector<std::string> vec = it->second.currentQuestion.getPossibleAnswers();
			if (vec.size() == 0)
			{
				gqr.status = 0;
				gqr.answers = std::map<unsigned int,std::string>();
			}
			else
			{
				gqr.status = 1;
				for (int i = 0; i < vec.size(); i++)
				{
					mapans.insert(std::pair<unsigned int, std::string>(i, vec[i]));
				}

				gqr.answers = mapans;
			}
			break;
				
		}
	}
	std::vector<uint8_t> myvector = jrps.serializeResponse(gqr);
	requestResult rr;
	rr.vec = returnfinalbu3(myvector, 12);
	rr.response = NULL;
	rr.newHandler = this;
	return rr;
	return rr;


}
requestResult GameRequestHandler::getGameResults(request r)
{
	GetGameResultsResponse ggrr;
	JsonResponsePacketSerializer jrps;
	std::map<LoggedUser, GameData>::iterator it = m_game->m_players.begin();
	bool flag = false;
	for (; it != m_game->m_players.end(); it++)
	{
		if (it->first.getUserName() == m_user.getUserName())
		{
			flag = true;
			double Tottime   = (clock() - begin_time) / CLOCKS_PER_SEC;
			Tottime = Tottime - (it->second.correctAnswerCount * CORRECT_SOUND_TIME) - (it->second.wrongAnswerCount * WRONG_SOUND_TIME);
			it->second.averangeAnswerTime = Tottime / (it->second.correctAnswerCount + it->second.wrongAnswerCount);
			m_gameManager->m_database->pushScore(it->second.correctAnswerCount, m_user.getUserName());
			m_game->removePlayer(m_user);
			break;
		}
		
	}
	if (m_game->m_players.size() == 0)
	{
		if (flag == true)//if it was the last player
		{
			m_gameManager->deleteGame(*m_game,m_user,&(m_handleFactory->m_roomManager));
		}
		ggrr.status = 1;
		ggrr.results = m_game->final_scores;
		requestResult rr;
		rr.response = NULL;
		rr.vec = returnfinalbu3(jrps.serializeResponse(ggrr),14);
		rr.newHandler = m_handleFactory->createMenuRequestHandler(m_user.getUserName());//for now
		return rr;
	}
	else
	{
		requestResult rr;
		ggrr.status = 0;
		ggrr.results =m_game->final_scores;
		rr.response = NULL;
		rr.vec = returnfinalbu3(jrps.serializeResponse(ggrr), 14);
		rr.newHandler = this;//for ever jk
		return rr;
	
	}


}

void GameRequestHandler::unexpected_logout()
{
	request r;

	r.buffer = (LPSTR)"";
	r.type = 999;
	requestResult rr = LeaveGame(r);
	rr.newHandler->unexpected_logout();
}