#pragma once
#include <string>
#include <iostream>
class LoggedUser
{
public:
	LoggedUser();
	LoggedUser(std::string name);
	std::string getUserName() const;
	void setUserName(std::string str);
	bool operator <(const LoggedUser const & lobj)
	{
		if (m_username[0] < lobj.m_username[0])
		{
			return true;
		}
		return false;
	}
private:
	std::string m_username;

};
