#include "Question.h"
Question::Question(std::string question, std::string correctAns, std::vector<std::string> possibleAnswers)
{
	m_question = question;
	m_correctAnswer = correctAns;
	m_possibleAnswers = possibleAnswers;
	m_possibleAnswers.push_back(correctAns);
}

std::string Question::getQuestion()
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return m_correctAnswer;
}

