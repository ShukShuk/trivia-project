#include "RequestHandleFactory.h"

RequestHandleFactory::RequestHandleFactory(Idatabase* db) : m_loginManager(db), m_roomManager(), m_gameManager(db)
{
	m_highscore = new Highscore(db);
}

loginRequestHandler * RequestHandleFactory::createLoginRequestHndler()
{
	loginRequestHandler * l = new  loginRequestHandler(&m_loginManager);
	l->m_requestHandlerFactory = this;
	return l;
}

MenuRequestHandler *  RequestHandleFactory::createMenuRequestHandler(std::string username)
{
	MenuRequestHandler* m = new MenuRequestHandler(username, &m_roomManager);
	m->m_handlefactory = this;
	m->m_highscoreTable = m_highscore;
	return m;
	
}

RoomAdminRequestHandler * RequestHandleFactory::createRoomAdminRequestHandler(LoggedUser l, Room * r)
{
	RoomAdminRequestHandler * rarh = new RoomAdminRequestHandler(r,l);
	rarh->m_handlerfactory = this;
	rarh->m_roommanager = &m_roomManager;
	return rarh;
}

RoomMemberRequestHandler * RequestHandleFactory::createRoomMemberRequestHandler(LoggedUser l, Room * r)
{
	RoomMemberRequestHandler * rmrh = new RoomMemberRequestHandler(r, l);
	rmrh->m_roommanager = &m_roomManager;
	rmrh->m_handlerfactory = this;
	return rmrh;
}

GameRequestHandler * RequestHandleFactory::createGameRequestHandler(Room * r, LoggedUser l)
{
	GameRequestHandler * grh = new GameRequestHandler(l, &m_gameManager, r, this);
	return grh;
}

