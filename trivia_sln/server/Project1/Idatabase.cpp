#include "Idatabase.h"
#include "Helper.h"
#include "RequestResult.h"
#include <string>
#include <fstream>
#include "jsonPacketDeserializer.h"
//int countCallback(void *data, int argc, char **argv, char **azColName)
//{
//	for (int i = 0; i < argc; i++)
//	{
//		if (std::string(azColName[i]) == "count")
//		{
//			if (argv[i] != NULL)
//			{
//				*(int*)data += atoi(argv[i]);
//			}
//		}
//	}
//
//	return 0;
//}
int highscoreCallback(void *data, int argc, char **argv, char **azcolname)
{
	std::string username;
	int score;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azcolname[i]) == "score")
		{
			if (argv[i] != NULL)
			{
				score = atoi(argv[i]);
			}
		}
		else if (std::string(azcolname[i]) == "username")
		{
			if (argv[i] != NULL)
			{
				username = std::string(argv[i]);
			}
		}
	}
	(*(std::map<int, LoggedUser>*)data).insert(std::pair<int, LoggedUser>(score, LoggedUser(username)));
	//(std::map<LoggedUser, int>*)
	return 0;
}

std::vector<QusetionDatafromWebSite> readFromFileQ(std::string path)
{
	std::vector<std::string> vec;
	helper h;
	h.sendemail("none", "none", "jsonReq.exe");
	std::string line;
	
	std::ifstream myfile(path);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			vec.push_back(line);
		}
		myfile.close();
	}
	std::vector<QusetionDatafromWebSite> afterd_vec;
	JsonRequestPacketDeserializer jrpd;
	afterd_vec.push_back(jrpd.deserializewebjson((LPSTR)vec[0].substr(1).c_str(),0));
	for (int i = 1; i < vec.size() - 1; i++)
	{
		if (vec[i][0] == '[')
		{
			vec[i] = vec[i].substr(1);
		}
		QusetionDatafromWebSite ans = jrpd.deserializewebjson((LPSTR)vec[i].c_str(), i);
		if (ans.type != "error")
		{
			afterd_vec.push_back(ans);
		}
	}
	afterd_vec.push_back(jrpd.deserializewebjson((LPSTR)vec[vec.size() -1].substr(0,vec[vec.size()-1].size() - 1).c_str(),1000));
	std::cout << afterd_vec.size();
	return afterd_vec;
}
Idatabase::Idatabase()
{
	int doesFileExist = _access(dbFileName, 0);
	int res = sqlite3_open(dbFileName, &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}
	if (doesFileExist == -1)
	{
		std::string pa = "questionsData.txt";
		std::vector<QusetionDatafromWebSite> questions = readFromFileQ(pa);
		const char* sqlStatement = "create table if not exists user(username text primary key, password text, email text, score integar); create table if not exists question(id integer primary key, question text, correct_ans text, ans2 text,ans3 text, ans4 text); create table if not exists game(id integer primary key, status integer, start_time date, end_time date);";
		char* errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << "DB error" << std::endl;
		}
		for (int i = 0; i < questions.size(); i++)
		{
			std::string strstate = "insert into question values(" + std::to_string(i) + ", \"" + questions[i].question + "\", \"" + questions[i].correct_answer + "\", \"" +  questions[i].incorrect_answers[0] + "\", \"" + questions[i].incorrect_answers[1] + "\", \"" + questions[i].incorrect_answers[2] + "\");";
			errMessage = nullptr;
			res = sqlite3_exec(db, strstate.c_str(), NULL, NULL, &errMessage);
			if (res != SQLITE_OK)
			{
				std::cout << "DB error" << std::endl;
			}
		}
	}
}

Idatabase::~Idatabase()
{
	sqlite3_close(db);
	db = nullptr;
}

bool Idatabase::doesUserExist(std::string str)
{
	//std::string sqlStatement = "select count(username) as count from users where username like " + str + ";";

	//char* errMessage = nullptr;
	//int album_count = 0;
	//int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &album_count, &errMessage);

	//return album_count != 0;
	return true;
}


int questionCallBack(void *data, int argc, char **argv, char **azColName)
{
	std::string qtemp ="";
	std::string catemp = "";
	std::vector<std::string> patemp;
	std::vector<Question> *vec = (std::vector<Question> *)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "id")
		{

		}
		else if (std::string(azColName[i]) == "question")
		{
			qtemp = std::string(argv[i]);
		}
		else if(std::string(azColName[i]) == "correct_ans")
		{
			catemp = std::string(argv[i]);
		}
		else
		{
			patemp.push_back(std::string(argv[i]));
		}
		
		
	}
	Question * q = new Question(qtemp, catemp, patemp);
	vec->push_back(*q);




	return 0;
}
std::vector<Question> Idatabase::getQuestions(int num)
{
	std::string sqlStatement = "select DISTINCT question, correct_ans, ans2, ans3, ans4 from question order by random() limit " + std::to_string(num) + ";";
	char * errMessage = nullptr;
	std::vector<Question> Qvec;
	int res = sqlite3_exec(db, sqlStatement.c_str(), questionCallBack, &Qvec, &errMessage);
	return Qvec;
}
std::map<int, LoggedUser> Idatabase::getHighscores()
{
	std::string sqlStatement = "select username, score from user order by score desc;";

	char* errMessage = nullptr;
	std::map<int, LoggedUser> userScores;
	int res = sqlite3_exec(db, sqlStatement.c_str(), highscoreCallback, &userScores, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage;
		system("pasue");
	}
	return userScores;
}
int scoreCallBack(void *data, int argc, char **argv, char **azColName)
{
	int score = 0 ;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "score")
		{
			score = atoi(argv[i]);
		}
	}
	*((int *)data) = score;





	return 0;
}
void Idatabase::pushScore(int score, std::string username)
{
	std::string sqlStatement = "select score from user where username like '" + username + "';";
	int currScore = 0;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(),scoreCallBack , &currScore, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage;
	}
	if (currScore < score)
	{
	
		sqlStatement = "UPDATE user SET score = " + std::to_string(score) + " WHERE username like '" + username + "';";
		errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), NULL, NULL, &errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << errMessage;
		}

	}
}

