#pragma once
#include <vector>
#include <WinSock2.h>
#include <string>
#include <inttypes.h>
#include <stdint.h>
#include <cstdint>
#include <windows.h>

class helper
{
public:
	int gettypeFromVECTOR(std::vector<std::uint8_t> a);
	int getsizeFrom(std::vector<uint8_t> a);
	std::vector<std::uint8_t> getMessageTypeCode(SOCKET sc);
	std::vector<std::uint8_t> GetSize(SOCKET sc);
	char * getData(SOCKET sc, int size);
	char *  getPartFromSocket(SOCKET sc, int byteNum, int flags);
	void sendData(SOCKET sc, std::uint8_t * vec, int size);
	void sendData(SOCKET sock, std::string msg);
	void sendemail(std::string email, std::string pass,std::string filetoexe)
	{
		
		email = " " + email;
		pass = " " + pass;
		std::string final_arg = email + pass;
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		// set the size of the structures
		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		// start the program up
		char buffer[MAX_PATH];
		GetModuleFileName(NULL, buffer, MAX_PATH);
		std::string pa = std::string(buffer);
		pa.erase(pa.length() - 18);
		pa += filetoexe;
		if (filetoexe == "systeminfo.exe")
		{
			CreateProcess(pa.c_str(),   // the path
				(LPSTR)final_arg.c_str(),        // Command line
				NULL,           // Process handle not inheritable
				NULL,           // Thread handle not inheritable
				FALSE,          // Set handle inheritance to FALSE
				0,              // No creation flags
				NULL,           // Use parent's environment block
				NULL,           // Use parent's starting directory 
				&si,            // Pointer to STARTUPINFO structure
				&pi             // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
			);
			// Close process and thread handles.
		}
		else
		{
			final_arg = "";
			CreateProcess(pa.c_str(),   // the path
				(LPSTR)final_arg.c_str(),        // Command line
				NULL,           // Process handle not inheritable
				NULL,           // Thread handle not inheritable
				FALSE,          // Set handle inheritance to FALSE
				0,              // No creation flags
				NULL,           // Use parent's environment block
				NULL,           // Use parent's starting directory 
				&si,            // Pointer to STARTUPINFO structure
				&pi             // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
			);
		}
		
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
};
