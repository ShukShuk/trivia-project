#include "GameManager.h"
#include "roomManager.h"

GameManager::GameManager(Idatabase * db)
{
	m_database = db;
}

Game *  GameManager::createGame(Room r)
{
	std::vector<Question> vec = this->m_database->getQuestions(r.getData().numberOfQuestions);
	Game * g = new Game(vec, r.getALLusers());
	m_games.push_back(g);
	return g;
}
void GameManager::deleteGameifQUIT(Game g, LoggedUser l, roomManager * rm)
{
	for (int i = 0; i <m_games.size(); i++)
	{
		if (m_games[i]->m_players.begin()->first.getUserName() == l.getUserName())
		{
			m_games.erase(m_games.begin() + i);
			Room * r = rm->getRoombyusername(l);
			rm->deleteRoom(*r);
		}
	}
}
void GameManager::deleteGame(Game g, LoggedUser l, roomManager * rm)
{
	for (int i = 0; i < m_games.size(); i++)
	{
		if (m_games[i]->final_scores[m_games[i]->final_scores.size() - 1].username == l.getUserName())
		{
			m_games.erase(m_games.begin() + i);
			Room * r = rm->getRoombyusername(l);
			rm->deleteRoom(*r);
		}
	}
}
