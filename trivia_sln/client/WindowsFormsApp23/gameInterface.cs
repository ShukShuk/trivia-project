﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft;
using static WindowsFormsApp23.roomdatac;
using static WindowsFormsApp23.getRoomState;
using static WindowsFormsApp23.GetQuestionStruct;
using static WindowsFormsApp23.playerResult;
using static WindowsFormsApp23.playerResultWITHstatus;
using WMPLib;


namespace WindowsFormsApp23
{
    public abstract class ABS_interface
    {
        public void createPanel()
        {
            mainpanel = new Panel();
            mainpanel.Size = form1.Size;
            mainpanel.BackColor = form1.BackColor;
            mainpanel.Location = form1.Location;
        }
        public Label createLabel(string text,int place)
        {
            Label new_label = new Label();
            new_label.Name = text;
            new_label.Text = text;
            new_label.ForeColor = Color.Green;
            new_label.Location = new Point(400, 100 + place * 100 +150);
            new_label.Size = new Size(200, 100);
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            new_label.Font = font;
            new_label.FlatStyle = FlatStyle.Popup;
            return new_label;
        }
        public void mouse_hover(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label.Focused == false)
            {
                label.Focus();
            }
        }
        public void label_enter(object sender, EventArgs e)
        {
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               40,
               FontStyle.Underline,
               GraphicsUnit.Pixel);
            Label L = (Label)sender;

            L.Font = font;

        }
        public void label_leave(object sender, EventArgs e)
        {
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            Label L = (Label)sender;
            L.Font = font;

        }

        public abstract void createLabels();
        

        protected Form1 form1;
        public Panel mainpanel;
        public bool isActive;
        public Label[] menu;
    }
    public class gamescreen : ABS_interface
    {
        private Button createKey(int place,int id)
        {
            Button button = new Button();
            button.Location = new Point(form1.Width /2  - 400 , 300 + place * 150);
            button.Text = _currQuestion.answers[id];
            button.Size = new Size(800, 100);
            button.BackColor = Color.Gray;
            button.Tag = id;
            FontFamily fontFamily = new FontFamily("Arial");
            Font f = new Font(fontFamily,
               56,
               FontStyle.Bold,
               GraphicsUnit.Pixel);
            button.Font = f;
            mainpanel.Controls.Add(button);
            return button;

        }
        private void setQuestions(GetQuestionStruct gqs)
        {
            FontFamily family = new FontFamily("Arial");
            Font titleF = new Font(family, 32, FontStyle.Regular, GraphicsUnit.Pixel);
            _currQuestion = gqs;
            Label l = new Label();
            l.Text = gqs.question;
            l.Size = new Size(800,150);
            l.ForeColor = Color.Green;
            l.Font = titleF;
            l.Location = new Point((form1.Width / 2) - (l.Width / 2),150);
            mainpanel.Controls.Add(l);
            currquestion = l;
            Random rand = new Random();
            _currQuestion.answers = _currQuestion.answers.OrderBy(x => rand.Next()).ToDictionary(item => item.Key, item => item.Value);
            int i = 0 ;
            foreach (KeyValuePair<int,string> entry in _currQuestion.answers)
            {
                Button temp = createKey(i, entry.Key);
                temp.Click += answerclicked;
                currbuttons.Add(temp);
                i++;
            }
        }
     
        private void answerclicked(object sender, EventArgs e)
        {
           
            
            


                m_timer.Stop();
                Button b = (Button)sender;
                int idOfB = int.Parse(b.Tag.ToString());
                b.FlatStyle = FlatStyle.Flat;
                for (int i = 0; i < 4; i++)
                {
                    currbuttons[i].Enabled = false;
                    if (currbuttons[i].Tag.ToString() == "3")
                    {
                        currbuttons[i].FlatStyle = FlatStyle.Flat;
                        currbuttons[i].FlatAppearance.BorderColor = Color.Green;
                        currbuttons[i].BackColor = Color.Green;
                    }
                }
                numberOfQUESTIONint++;
                WindowsMediaPlayer windowsMediaPlayer = new WindowsMediaPlayer();
                Player = windowsMediaPlayer;
                if (form1.getCorrectAnswerPlusSubmit(idOfB) == idOfB)
                {

                    b.FlatAppearance.BorderColor = Color.Green;
                    b.BackColor = Color.Green;
                    windowsMediaPlayer.PlayStateChange += WindowsMediaPlayer_PlayStateChange;
                    windowsMediaPlayer.URL = form1.pathtoDGLOBAL + "cor.mp3";
                    windowsMediaPlayer.controls.play();
                }
                else
                {
                    windowsMediaPlayer.PlayStateChange += WindowsMediaPlayer_PlayStateChange;

                    windowsMediaPlayer.URL = form1.pathtoDGLOBAL + "wro.mp3";
                    windowsMediaPlayer.controls.play();
                    b.FlatAppearance.BorderColor = Color.Red;
                    b.BackColor = Color.Red;
                }
            
            //GetQuestionStruct gqr =  form1.getQuestionFromServer();
            //_numberOfQuestion.Text = "question " + numberOfQUESTIONint.ToString() + "from " + _roomdata.questionCount.ToString();
            
            //foreach (var entry in currbuttons)
            //{
            //    mainpanel.Controls.Remove(entry);
            //}
            //mainpanel.Controls.Remove(currquestion);
            //setQuestions(gqr);

            //_interval_counter = _roomdata.answerTimeout;

        }

        private void WindowsMediaPlayer_PlayStateChange(int NewState)
        {
            if (NewState == 8)
            {
                if (numberOfQUESTIONint > _roomdata.questionCount)
                {
                    form1.Controls.Remove(mainpanel);
                    createPanel();
                    playerResultWITHstatus wITHstatus = form1.we_are_in_the_endgame_now();
                    if(wITHstatus.status == 1)
                    {
                        Label fin = new Label();
                        fin.AutoSize = true;
                        fin.Text = "final results";
                        fin.Size = new Size(800,100);
                        fin.Font = _fontForEVERYONE;
                        fin.Location = new Point((form1.Width / 2)  - (fin.Width /2), 0);
                        fin.ForeColor = Color.Green;
                        Button quit = new Button();
                        quit.Text = "quit";
                        quit.Font = _fontForEVERYONE;
                        quit.Size = new Size(200, 200);
                        quit.Location = new Point(form1.Width - 200, form1.Height - 200);
                        quit.Click += Quit_Click;
                        quit.ForeColor = Color.Green;
                        FlowLayoutPanel layoutPanel = new FlowLayoutPanel();
                        layoutPanel.Size = new Size(800, 400);
                        
                        layoutPanel.AutoScroll = true;
                        layoutPanel.BackColor = Color.Black;
                        layoutPanel.Location = new Point(200, 200);
                        layoutPanel.FlowDirection = FlowDirection.TopDown;
                        
                        foreach (playerResult entry in wITHstatus.results)
                        {
                            Label l = new Label();
                            l.AutoSize = true;
                            l.Size = new Size(800,100 );
                            l.ForeColor = Color.Green;
                            l.Text = "Username: " + entry.username + "\t CorrectAnswer: " + entry.correctAnswerCount + "\t WrongAnswer: " + entry.wrongAnswerCount + "\t AverageTimePerAnswer: " + entry.averageAnswerTime;
                            l.Font = _fontForEVERYONE;
                            layoutPanel.Controls.Add(l);
                        }
                        mainpanel.Controls.Add(layoutPanel);
                        mainpanel.Controls.Add(quit);
                        mainpanel.Controls.Add(fin);
                    }
                    else
                    {
                        m_timer.Start();
                        Label wfp = new Label();
                        wfp.Text = "waiting for players to finish";
                        wfp.Size = new Size(400, 100);
                        wfp.Font = _fontForEVERYONE;
                        wfp.Location = new Point((form1.Width / 2) - (wfp.Width / 2), 0);
                        wfp.ForeColor = Color.Green;
                        mainpanel.Controls.Add(wfp);

                    }
                    form1.Controls.Add(mainpanel);
                    //get game result 
                    //launch the last screen(score)?
                    //return to main menu

                }
                else
                {


                    GetQuestionStruct gqr = form1.getQuestionFromServer();
                    _numberOfQuestion.Text = "question " + numberOfQUESTIONint.ToString() + "from " + _roomdata.questionCount.ToString();

                    foreach (var entry in currbuttons)
                    {
                        mainpanel.Controls.Remove(entry);
                    }
                    mainpanel.Controls.Remove(currquestion);
                    currbuttons.Clear();
                    setQuestions(gqr);

                    _interval_counter = _roomdata.answerTimeout;
                    _timerLabel.Text = _interval_counter.ToString();
                    m_timer.Start();

                }
            }
        }

        public gamescreen(getRoomState grs, object sender)
        {
            currbuttons = new List<Button>();
            mainpanel = new Panel();
            form1 = (Form1)sender;
            form1.avlab.Visible = false;
            form1.userlabel.Visible = false;
            mainpanel.Size = form1.Size;
            _roomdata = grs;
            Timer t = new Timer();
            t.Interval =1000;
            m_questionCount = grs.questionCount;
            numberOfQUESTIONint = 1;
            m_timer = t;
            m_timer.Tick += M_timer_Tick;
            _numberOfQuestion = new Label();
            _numberOfQuestion.Text = "question 1 from" +m_questionCount.ToString();
            _timerLabel = new Label();
            _timerLabel.AutoSize = true;
            _numberOfQuestion.AutoSize = true;
            _timerLabel.Text = grs.answerTimeout.ToString();
            createLabels();
            leaveGame = new Button();
            leaveGame.ForeColor = Color.Green;
            leaveGame.Location = new Point(0, 0);
            leaveGame.Size = new Size(200, 100);
            leaveGame.Text = "leave game";
            leaveGame.Click += LeaveGame_Click;
            mainpanel.Controls.Add(leaveGame);
            form1.Controls.Add(mainpanel);
            GetQuestionStruct getQuestion = form1.getQuestionFromServer();
            setQuestions(getQuestion);
            _interval_counter = _roomdata.answerTimeout;
            m_timer.Start();
        }

        private void LeaveGame_Click(object sender, EventArgs e)
        {
            form1.LeaveGame();
            m_timer.Stop();
            form1.Controls.Remove(mainpanel);
            form1.avlab.Visible = true;
            form1.userlabel.Visible = true;
            MAINmenu mAI = new MAINmenu(form1);
        }

        private void M_timer_Tick(object sender, EventArgs e)
        {
            if (numberOfQUESTIONint > _roomdata.questionCount)
            {

                playerResultWITHstatus wITHstatus = form1.we_are_in_the_endgame_now();
                if (wITHstatus.status == 1)
                {
                    form1.Controls.Remove(mainpanel);
                    createPanel();
                    m_timer.Stop();
                    Label fin = new Label();
                    fin.AutoSize = true;
                    fin.Text = "final results";
                    fin.Size = new Size(400, 100);
                    fin.Font = _fontForEVERYONE;
                    fin.Location = new Point((form1.Width / 2) - (fin.Width / 2), 0);
                    fin.ForeColor = Color.Green;
                    mainpanel.Controls.Add(fin);
                    Button quit = new Button();
                    quit.Text = "quit";
                    quit.Font = _fontForEVERYONE;
                    quit.Size = new Size(200, 200);
                    quit.Location = new Point(form1.Width - 200, form1.Height - 200);
                    quit.Click += Quit_Click;
                    quit.ForeColor = Color.Green;
                    FlowLayoutPanel layoutPanel = new FlowLayoutPanel();
                    layoutPanel.BackColor = Color.Gray;
                    layoutPanel.Size = new Size(800, 400);
                    layoutPanel.AutoScroll = true;
                    layoutPanel.BackColor = Color.Black;
                    layoutPanel.Location = new Point(200, 200);
                    layoutPanel.FlowDirection = FlowDirection.TopDown;
                    foreach (playerResult entry in wITHstatus.results)
                    {
                        Label l = new Label();
                        l.AutoSize = true;
                        l.Size = new Size(800, 100);
                        l.ForeColor = Color.Green;
                        l.Text = "Username: " + entry.username + "     CorrectAnswer: " + entry.correctAnswerCount + "     WrongAnswer: " + entry.wrongAnswerCount + "     AverageTimePerAnswer: " + entry.averageAnswerTime;
                        l.Font = _fontForEVERYONE;
                        layoutPanel.Controls.Add(l);
                    }
                    mainpanel.Controls.Add(layoutPanel);
                    mainpanel.Controls.Add(quit);
                    form1.Controls.Add(mainpanel);
                }
                
                
                    
                    
                
                //get game result 
                //launch the last screen(score)?
                //return to main menu
            }
            else
            {


                _interval_counter--;

                if (_interval_counter <= 0)
                {
                    Button b = new Button();
                    b.Click += answerclicked;
                    b.Tag = 99;
                    b.PerformClick();
                    _interval_counter = _roomdata.answerTimeout;

                }
                _timerLabel.Text = _interval_counter.ToString();
            }
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            m_timer.Stop();
            form1.Controls.Remove(mainpanel);
            try
            {
                form1.avlab.Visible = true;
                form1.userlabel.Visible = true;
            }
            catch
            {

            }
            MAINmenu mAI = new MAINmenu(form1);
        }

        public override void createLabels()
        {
            _timerLabel.Size = new Size(300, 300);
            _timerLabel.ForeColor = Color.Green;
            FontFamily fontFamily = new FontFamily("Arial");
            Font f = new Font(fontFamily,
               56,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            _fontForEVERYONE = f;
            _timerLabel.Font = f;
            _timerLabel.Location = new Point((form1.Width / 2) - (_numberOfQuestion.Width / 2), 0);
            mainpanel.Controls.Add(_timerLabel);
            _numberOfQuestion.Size = new Size(300, 300);
            _numberOfQuestion.Location = new Point(_timerLabel.Location.X + _timerLabel.Width + 200, _timerLabel.Location.Y);
            _numberOfQuestion.Font = f;
            _numberOfQuestion.ForeColor = Color.Green;
            mainpanel.Controls.Add(_numberOfQuestion);
        }
        Button leaveGame;
        Label _numberOfQuestion;
        Label _timerLabel;
        int _interval_counter;
        getRoomState _roomdata;
        int m_questionCount;
        int numberOfQUESTIONint;
        Timer m_timer;
        GetQuestionStruct _currQuestion;
        Font _fontForEVERYONE;
        List<Button> currbuttons;
        Label currquestion;
        WindowsMediaPlayer Player;
        List<playerResult> playerResults;
    }
    public class lobby :ABS_interface
    {
        public lobby(object sender, List<string> ls, bool i, roomdatac rc)
        {
            _roomdata = rc;

            isHost = i;
            form1 = (Form1)sender;
            _username = form1.userlabel.Text;
            _players = ls;

            UserControl1 control1 = new UserControl1();

            m_usercontrol = control1;
            control1.Size = form1.Size;
            control1.resizepic();
            form1.Controls.Add(control1);
            form1.killavlab();
            //form1.Controls.Remove(form1.userlabel);
            form1.userlabel.Visible = false;
            m_usercontrol.addtolist(_players, _username);
           MetroFramework.Controls.MetroButton leave = new MetroFramework.Controls.MetroButton ();
            leave.Text = "Leave";
            leave.Font = new Font("Arial", 30, FontStyle.Bold);
            leave.ForeColor = Color.Green;
            leave.Click += Leave_Click;
            m_usercontrol.addbutton(leave);
            if (isHost)
            {
                MetroFramework.Controls.MetroButton start = new MetroFramework.Controls.MetroButton();
                start.Text = "Start";
                start.Font = new Font("Arial", 30, FontStyle.Bold);
                start.ForeColor = Color.Green;
                start.Click += Start_Click;
                m_usercontrol.addbutton(start);

            }
            Timer t = new Timer();
                t.Interval = 1000;
                t.Tick += T_Tick;
                t.Start();
            m_timer = t;
            
         
        }

        private void T_Tick(object sender, EventArgs e)
        {
           getRoomState grs =  form1.getdataforroom();
            if(!isHost)
            {
                if(grs.status ==0)
                {
                    //return to menu ,aka leave room
                    form1.avlab.Visible = true;
                    form1.userlabel.Visible = true;
                    form1.Controls.Remove(m_usercontrol);
                    MAINmenu mAI = new MAINmenu(form1);
                }
                else if (grs.hasGameBegun)
                {
                    //next screen
                    m_timer.Stop();
                    form1.Controls.Remove(m_usercontrol);
                    gamescreen gs = new gamescreen(grs, form1);
                }
                
               
            }
            _players = grs.ls;
            m_usercontrol.updateList(_players, _username);
            
        }

        private void Start_Click(object sender, EventArgs e)
        {
            m_timer.Stop();
           if(form1.startRoom())
            {

                getRoomState state = new getRoomState();
                state.ls = _players;
                state.questionCount = _roomdata.questionCount;
                state.status = 1;
                state.answerTimeout = _roomdata.timePerQuestion;
                state.hasGameBegun = true;
                form1.avlab.Visible = true;
                form1.userlabel.Visible = true;
                form1.Controls.Remove(m_usercontrol);
                gamescreen gs = new gamescreen(state, form1);
            }
        }

        private void Leave_Click(object sender, EventArgs e)
        {
            m_timer.Stop();
                if (form1.closeRoom())
                {
                    form1.avlab.Visible = true;
                    form1.userlabel.Visible = true;
                    form1.Controls.Remove(m_usercontrol);
                    MAINmenu mAI = new MAINmenu(form1);
                }
            
           
        }

        public override void createLabels()
        {
            throw new NotImplementedException();
        }
        private string _username;
        public List<string> _players;
       public Form1 form1;
        public roomdatac _roomdata;
        private UserControl1 m_usercontrol;
        bool isHost;
        private Timer m_timer;
    }
    public class Rooms : ABS_interface
    {
        public FlowLayoutPanel _maintable;
        public void init_table(List<roomdatac> roomdatacs)
        {
            
            foreach (roomdatac entry in roomdatacs)
            {

                Button join = new Button();
                join.Text = "JOIN";
                join.FlatStyle = FlatStyle.Flat;
                join.BackColor = Color.LightBlue;
                join.ForeColor = Color.DarkGreen;
                FlowLayoutPanel subflow = new FlowLayoutPanel();
                subflow.Size = new Size(_maintable.Width, 75);
                subflow.BackColor = Color.LightBlue;
                Label s = new Label();
                s.AutoSize = true;
                s.Text = "id =    " + entry.id + ",    name =     " + entry.name + ",    number of questions =    " + entry.questionCount + ",    timeout =    " + entry.timePerQuestion + ",    players= " + entry.currplayers + "/" + entry.maxplayers;
                if (entry.isactive == 1)
                    s.Text += ",    ongoing";
                else
                {
                    s.Text += ",   ready to join";

                }
                FontFamily fontFamily = new FontFamily("Arial");
                Font font = new Font(
                   fontFamily,
                   16,
                   FontStyle.Bold,
                   GraphicsUnit.Pixel);
                s.Font = font;
                s.ForeColor = Color.DarkGreen;
                if (entry.isactive == 1 || entry.maxplayers <= entry.currplayers)
                {
                    join.Enabled = false;
                }
                join.Click += joinroom;
                join.Tag = entry.id;
                subflow.Controls.Add(s);
                subflow.Controls.Add(join);
                _tablep.Add(entry.id, subflow);
                _tabledata.Add(entry.id, entry);
                _maintable.Controls.Add(subflow);
               

            }
        }
            public Rooms(object sender)
        {
            _tabledata = new Dictionary<int, roomdatac>();
            Timer t = new Timer();
            t.Interval = 1000 * 10;// 10 sec
            t.Tick += T_Tick;
            t.Start();
            _tablep = new Dictionary<int, FlowLayoutPanel>();
            _timerforrooms = t;
            form1 = (Form1)sender;
            Button b = new Button();
            mainpanel = new Panel();
            mainpanel.Size = form1.Size;
            createLabels();
            FlowLayoutPanel mainflow = new FlowLayoutPanel();
            mainflow.Location = new Point(100, 200);
            mainflow.Size = new Size(1000, 500);
            mainflow.AutoScroll = true;
            mainflow.BackColor = Color.Gray;
            mainflow.FlowDirection = FlowDirection.TopDown;
            _maintable = mainflow;
            b.Text = "back";
            b.Location = new Point(mainflow.Location.X, mainflow.Location.Y + mainflow.Height);
            b.Click += backfromjoin;
            b.ForeColor = Color.DarkGreen;
            b.Width += 100;
            mainpanel.Controls.Add(b);
            List<roomdatac> roomdatas = form1.GetRooms();
            _rooms = roomdatas;
            init_table(roomdatas);
            mainpanel.Controls.Add(mainflow);
            form1.Controls.Add(mainpanel);


        }

        private void T_Tick(object sender, EventArgs e)
        {
            List<roomdatac> roomdatas = form1.GetRooms();
            _rooms.Clear();
            _rooms = roomdatas;
            foreach(KeyValuePair<int,FlowLayoutPanel> entry in _tablep)
            {
                _maintable.Controls.Remove(entry.Value);
                
            };
            _tablep.Clear();
            _tabledata.Clear();
            init_table(roomdatas);
                
            
        }

        private void backfromjoin(object sender, EventArgs e)
        {
            _timerforrooms.Stop();
            form1.Controls.Remove(mainpanel);
            MAINmenu mAI = new MAINmenu(form1);
        }

        private void joinroom(object sender, EventArgs e)
        {
            _timerforrooms.Stop();
            Button b = (Button)sender;
            string ids = b.Tag.ToString();
            int id = int.Parse(ids);
            roomdatac dataforid = new roomdatac();
            foreach (KeyValuePair<int,roomdatac> entry in _tabledata)
            {
                if(entry.Key == id)
                {
                    dataforid = entry.Value;
                    break;
                }
            }
            
            bool ans = form1.joinRoomById(id);
            if(ans)
            {
                List<string> ls = form1.getplayersbyId(id);
                form1.Controls.Remove(mainpanel);
                lobby lobby = new lobby(form1,ls,false,dataforid) ;
            }
            else
            {
                _timerforrooms.Start();
            }
        }

        public override void createLabels()
        {
            
        }
        public Timer _timerforrooms;
        public List<roomdatac> _rooms;
        public Dictionary<int,FlowLayoutPanel> _tablep;
        public Dictionary<int, roomdatac> _tabledata;
    };
    public class createroomObject : ABS_interface
    {
        protected void textBox2_SetText(TextBox text)
        {
            text.Text = "20";
            text.ForeColor = Color.DarkRed;
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (((TextBox)sender).ForeColor == Color.Black)
                return;
            ((TextBox)sender).Text = "";
            ((TextBox)sender).ForeColor = Color.Black;
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text.Trim() == "")
                textBox2_SetText(((TextBox)sender));
        }
        public override void createLabels()
        {
            Label title = createLabel("Create label", 0);
            mainpanel.Controls.Add(title);
            Label room_name = createLabel("enter name", 1);
            mainpanel.Controls.Add(room_name);
            Label maxUsers = createLabel("maxUser", 2);
            mainpanel.Controls.Add(maxUsers) ;
            Label questionCount = createLabel("number of questions:", 3);
            mainpanel.Controls.Add(questionCount);
            Label answertimeout = createLabel("enter answer timeout:", 4);
            mainpanel.Controls.Add(answertimeout);
            TextBox room_nametb = new TextBox();
            room_nametb.Enter += new EventHandler(textBox2_Enter);
            room_nametb.Leave += new EventHandler(textBox2_Leave);
            textBox2_SetText(room_nametb);
            room_nametb.Location = new Point(550, 100 + 1 * 100 + 150);
            TextBox maxUserstb = new TextBox();
            maxUserstb.Enter += new EventHandler(textBox2_Enter);
           maxUserstb.Leave += new EventHandler(textBox2_Leave);
            textBox2_SetText(maxUserstb);
            maxUserstb.Location = new Point(550, 100 + 2 * 100 + 150);
            TextBox questioncounttb = new TextBox();
            questioncounttb.Enter += new EventHandler(textBox2_Enter);
            questioncounttb.Leave += new EventHandler(textBox2_Leave);
            textBox2_SetText(questioncounttb);
            questioncounttb.Location = new Point(550, 100 + 3 * 100 + 150);
            TextBox answertb = new TextBox();
            answertb.Enter += new EventHandler(textBox2_Enter);
            answertb.Leave += new EventHandler(textBox2_Leave);
            textBox2_SetText(answertb);
            answertb.Location = new Point(550, 100 + 4 * 100 + 150);
            tbdict.Add("name", room_nametb);
            tbdict.Add("maxUsers", maxUserstb);
            tbdict.Add("questioncount", questioncounttb);
            tbdict.Add("answertb", answertb);

        }
        public createroomObject(object sender)
        {
            tbdict = new Dictionary<string, TextBox>();
            form1 = (Form1)sender;
            mainpanel = new Panel();
            mainpanel.Size = form1.Size;
            createLabels();
            Button b = new Button();
            b.Location = new Point(300, 100 + 5 * 100 + 150);
            b.Size = new Size(300, 100);
            b.ForeColor = Color.Green;
            b.Text = "ENTER";
            Button b1 = new Button();
            b1.Location = new Point(600, 100 + 5 * 100 + 150);
            b1.Size = new Size(300, 100);
            b1.ForeColor = Color.Green;
            b1.Text = "back";
            b.Click += B_Click;
            b1.Click += B1_Click;
            mainpanel.Controls.Add(b);
            mainpanel.Controls.Add(b1);
            foreach (KeyValuePair<string, TextBox> entry in tbdict)
            {
                mainpanel.Controls.Add(entry.Value);
                entry.Value.Width += 100;
                entry.Value.Location = new Point(entry.Value.Location.X + 100, entry.Value.Location.Y);
            }
            form1.Controls.Add(mainpanel);
        }
        
        private void B1_Click(object sender, EventArgs e)
        {
            form1.Controls.Remove(mainpanel);
            
            MAINmenu mAI = new MAINmenu(form1) ;
        }

        private void B_Click(object sender, EventArgs e)
        {
            bool b = form1.sendroomdata(tbdict["name"].Text, tbdict["maxUsers"].Text, tbdict["questioncount"].Text, tbdict["answertb"].Text);
            if (!b)
            {
                MessageBox.Show("error, wrong input or something");
            }
            else
            {
                roomdatac rc = new roomdatac();
                rc.currplayers = 1;
                rc.maxplayers = int.Parse(tbdict["maxUsers"].Text);
                rc.name = tbdict["name"].Text;
                rc.questionCount = int.Parse(tbdict["questioncount"].Text);
                rc.timePerQuestion = int.Parse(tbdict["answertb"].Text);
  
                form1.Controls.Remove(mainpanel);
                List<string> lis= new List<string>();
                lis.Add(form1.userlabel.Text);
                lobby l = new lobby(form1, lis,true,rc);
            }
        }
        private Dictionary<string, TextBox> tbdict; 
    };
    public class MAINmenu : ABS_interface
    {
        public string[] labelsnames = { "join room", "create room", "my status","best scores","exit","logout"};
       public MAINmenu(object sender)
        {
            form1 = (Form1)sender;
            mainpanel = new Panel();
            mainpanel.Size = form1.Size;
            mainpanel.BackColor = form1.BackColor;
            menu = new Label[6];
            createLabels();
            form1.Controls.Add(mainpanel);
            menu[0].Focus();
        }
        public override void createLabels()
        {
            for (int i = 0;i< labelsnames.Length;i++)
            {
                menu[i] = createLabel(labelsnames[i], i);
                switch(i)
                {
                    case 0:
                        menu[i].Click += joinroomclick;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;
                    case 1:
                        menu[i].Click += createroomclick;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;
                    case 2:
                        menu[i].Click += mystatusclick;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;
                    case 3:
                        menu[i].Click += bestcoresclick;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;
                    case 4:
                        menu[i].Click += back_click;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;
                    case 5:
                        menu[i].Click += logoutclick;
                        menu[i].Enter += label_enter;
                        menu[i].Leave += label_leave;
                        menu[i].MouseHover += mouse_hover;
                        mainpanel.Controls.Add(menu[i]);
                        break;

                }
            }

        }

        private void logoutclick(object sender, EventArgs e)
        {
            form1.Controls.Remove(mainpanel);
            bool ans = form1.logout();
            form1.Form1_Load(form1, EventArgs.Empty);
        }

        private void MAINmenu_MouseHover(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void back_click(object sender, EventArgs e)
        {
            

            //close the pipe   
            Application.Exit();
        }

        private void bestcoresclick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void mystatusclick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void createroomclick(object sender, EventArgs e)
        {
            form1.Controls.Remove(mainpanel);
            createroomObject createroom = new createroomObject(form1);

        }

        private void joinroomclick(object sender, EventArgs e)
        {
            form1.Controls.Remove(mainpanel);
            Rooms r = new Rooms(form1);

        }

    }
}
