﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp23
{
    public class roomdatac
    {
        public int id;
        public string name;
        public int maxplayers;
        public int timePerQuestion;
        public int questionCount;
        public int isactive;
        public int currplayers;
    }
    public class playernamec
    {
        public string playerName;
    }
    
    public class GetQuestionStruct
    {
      public  int status;
       public string question;
       public Dictionary<int, string> answers;
    }
    public class playerResult
    {
        public string username;
        public int correctAnswerCount;
        public int wrongAnswerCount;
        public int averageAnswerTime;
    }
    public class playerResultWITHstatus
    {
        public int status;
        public List<playerResult> results;
    }
}
