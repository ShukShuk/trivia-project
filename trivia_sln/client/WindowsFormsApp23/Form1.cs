﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using Microsoft.VisualBasic;
using Newtonsoft;
using System.Net.Sockets;
using System.Web.Script.Serialization;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using static WindowsFormsApp23.roomdatac;
using static WindowsFormsApp23.getRoomState;
using static WindowsFormsApp23.GetQuestionStruct;
using static WindowsFormsApp23.playerResult;
using static WindowsFormsApp23.playerResultWITHstatus;

namespace WindowsFormsApp23
{
    public partial class Form1 : Form
    {
        public Label avlab;
        public Label userlabel;
        public string pathtoDGLOBAL;
        Label avlabtemp;
        bool ft = false;
        WindowsMediaPlayer auidoeffect = new WindowsMediaPlayer();
        [DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbfont, uint cbfont, IntPtr pdv, [In] ref uint pcFonts);
        FontFamily ff;
        Font avfont;
        LinkedList<Panel> lpanels = new LinkedList<Panel>();
        TcpClient client;
        bool into = false;
        Panel[] panels = new Panel[2];
        WindowsMediaPlayer globalplyer = new WindowsMediaPlayer();
        Label[] menulabels;
        TextBox tb;
        Panel game;
        bool connected;
        string temp;

        
        string pathoffolder;
        public Form1()
        {
            InitializeComponent();
            Cursor.Hide();
            try
            {
                client = new TcpClient();
                client.Connect("127.0.0.1", 6743);
            }
            catch
            {

            }
        }
        private void load_font()
        {
            byte[] fontArray = Properties.Resources.AVENGEANCE;
            int dataLength = Properties.Resources.AVENGEANCE.Length;
            IntPtr ptrData = Marshal.AllocCoTaskMem(dataLength);
            Marshal.Copy(fontArray, 0, ptrData, dataLength);
            uint cFonts = 0;
            AddFontMemResourceEx(ptrData, (uint)fontArray.Length, IntPtr.Zero, ref cFonts);

            PrivateFontCollection pfc = new PrivateFontCollection();

            pfc.AddMemoryFont(ptrData, dataLength);
            Marshal.FreeCoTaskMem(ptrData);
            ff = pfc.Families[0];
            avfont = new Font(ff, 40f, FontStyle.Bold);

        }


        private void create_menu()
        {
            Label options = new Label();
            options.Name = "options";
            options.Text = "options";
            options.ForeColor = Color.Green;
            options.Location = new Point(200, 300);
            options.Size = new Size(200, 100);
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            options.Font = font;
            options.FlatStyle = FlatStyle.Popup;
            options.Enter += label1_Enter;
            options.Click += Options_Click;
            menulabels[1] = options;
            panels[0].Controls.Add(menulabels[1]);
            Label info = new Label();
            info.Name = "info";
            info.Text = "info";
            info.ForeColor = Color.Green;
            info.Location = new Point(200, 400);
            info.Size = new Size(200, 100);
            info.Font = font;
            info.FlatStyle = FlatStyle.Popup;
            info.Enter += label1_Enter;
            info.Click += Info_Click;
            menulabels[2] = info;
            panels[0].Controls.Add(menulabels[2]);
            Label quit = new Label();
            quit.Name = "quit";
            quit.Text = "quit";
            quit.ForeColor = Color.Green;
            quit.Location = new Point(200, 500);
            quit.Size = new Size(200, 100);
            quit.Font = font;
            quit.FlatStyle = FlatStyle.Popup;
            quit.Enter += label1_Enter;
            quit.Click += Quit_Click;
            menulabels[3] = quit;
            panels[0].Controls.Add(menulabels[3]);

        }
        private void createoptionspanel()
        {
            Panel options = new Panel();
            panels[1] = options;
            options.Location = this.Location;
            options.BackColor = this.BackColor;
            options.Size = this.Size;
            menulabels = new Label[3];
            Label volume = new Label();
            volume.Name = "volume";
            volume.Text = "volume";
            volume.ForeColor = Color.Green;
            volume.Location = new Point(200, 200);
            volume.Size = new Size(200, 100);
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            volume.Font = font;
            volume.FlatStyle = FlatStyle.Popup;
            volume.Enter += label1_Enter;
            volume.Click += Volume_Click;
            volume.MouseHover += hover_func;
            menulabels[0] = volume;
            panels[1].Controls.Add(menulabels[0]);
            Label changesong = new Label();
            changesong.Name = "change song";
            changesong.Text = "change song";
            changesong.ForeColor = Color.Green;
            changesong.Location = new Point(200, 300);
            changesong.Size = new Size(200, 100);
            changesong.Font = font;
            changesong.FlatStyle = FlatStyle.Popup;
            changesong.Enter += label1_Enter;
            changesong.Click += Changesong_Click;
            changesong.MouseHover += hover_func;
            menulabels[1] = changesong;
            panels[1].Controls.Add(changesong);
            Label back = new Label();
            back.Name = "back";
            back.Text = "back";
            back.ForeColor = Color.Green;
            back.Location = new Point(200, 400);
            back.Size = new Size(200, 100);
            back.Font = font;
            back.FlatStyle = FlatStyle.Popup;
            back.Enter += label1_Enter;
            back.Click += Back_Click;
            back.MouseHover += hover_func;
            menulabels[2] = back;
            panels[1].Controls.Add(back);
            this.Controls.Add(panels[1]);
            for (int i = 0; i < menulabels.Length; i++)
            {
                menulabels[i].Leave += label1_Leave;

            }
            menulabels[0].Focus();
            
        }

        private void hover_func(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label.Focused == false)
            {
                label.Focus();
            }
        }


        private void Volume_Click(object sender, EventArgs e)
        {
            auidoeffect.controls.play();
            try
            {
                //string ans = Microsoft.VisualBasic.Interaction.InputBox("enter volume", "set volume", "enter here");
                int vol = int.Parse(textBox1.Text);
                globalplyer.settings.volume = vol;
            }
            catch
            {

            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
            auidoeffect.controls.play();
            textBox1.Visible = false;
            this.Controls.Remove(panels[1]);
            panels[1].Visible = false;
            Form1_Load(this, EventArgs.Empty);
        }

        private void Changesong_Click(object sender, EventArgs e)
        {

            auidoeffect.controls.play();
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string file_name = fileDialog.FileName;
                    globalplyer.controls.stop();
                    globalplyer.URL = file_name;
                    globalplyer.controls.play();
                }
                catch
                {

                }
            }
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            auidoeffect.controls.play();
            Application.Exit();
        }

        private void Options_Click(object sender, EventArgs e)
        {

            //auidoeffect.controls.play();
            //panels[0].Visible = false;
            //this.Controls.Remove(panels[0]);
            //createoptionspanel();
            //textBox1.Visible = true;
            MessageBox.Show("coming soon, ps visit http://guyforceshiswifetodressinagarbagebagforthenextthreeyears.com");

        }

        private void Info_Click(object sender, EventArgs e)
        {
            auidoeffect.controls.play();

            AboutBox1 aboutBox1 = new AboutBox1();
            aboutBox1.Show();

        }

        public void Form1_Load(object sender, EventArgs e)
        {


            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            load_font();
            pathoffolder = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            pathoffolder = pathoffolder.Remove(pathoffolder.Length - 9);
            temp = pathoffolder;
            string forwinvid = temp;
            pathtoDGLOBAL = forwinvid;
            pathoffolder += "mpstarter.mp3";
            temp += "snap.mp3";


            if (into == true)
            {
            
                auidoeffect.URL = temp;
                try
                {
                   pictureBox1.Size = new Size(500, 500);
                    pictureBox1.Location = new Point(this.Width - 500, this.Height - 400);
                    pictureBox1.Visible = true;
                }
                catch
                {
                    Application.Exit();
                }
            }

            if (into == false)
            {
                axWindowsMediaPlayer1.Visible = true;
                axWindowsMediaPlayer1.URL = forwinvid + "nmi.mp4";
                axWindowsMediaPlayer1.settings.autoStart = true;
                axWindowsMediaPlayer1.PlayStateChange += AxWindowsMediaPlayer1_PlayStateChange;
                into = true;

            }




            PictureBox pictureBox = new PictureBox() { Image = Image.FromFile(forwinvid + "cursor.png") };
            this.Cursor = new Cursor(((Bitmap)pictureBox.Image).GetHicon());

            Panel menu = new Panel();
            menulabels = new Label[4];
            this.KeyPreview = true;
            this.BackColor = Color.Black;
            menu.Location = this.Location;
            menu.Size = this.Size;
            menu.BackColor = this.BackColor;
            Label l = new Label();
            l.Name = "start";
            l.Text = "start";
            l.ForeColor = Color.Green;
            l.Location = new Point(200, 200);
            l.Size = new Size(200, 100);

            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            l.Font = font;
            l.FlatStyle = FlatStyle.Popup;
            l.Enter += label1_Enter;
            l.Click += L_Click;
            menulabels[0] = l;
            menu.Controls.Add(l);
            panels[0] = menu;
            create_menu();
            for (int i = 0; i < menulabels.Length; i++)
            {
                menulabels[i].Leave += label1_Leave;
                menulabels[i].MouseHover += hover_func;
            }
            this.Controls.Add(menu);
            l.Focus();
          // this.Controls.Remove(avlab);
           
      
            this.Controls.Remove(userlabel);
            Label name = new Label();
            name.Font = avfont;
            name.Size = new Size(400, 100);
            name.Location = new Point(1000,100);
            name.Text = "";
            name.Visible = false;
            name.AutoSize = true;
            name.ForeColor = Color.Blue;
            name.Visible = false;
            name.BackColor = Color.Transparent;
            userlabel = name;
            userlabel.Visible = false;
            this.Controls.Add(userlabel);

        }

        private void AxWindowsMediaPlayer1_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {

            if (axWindowsMediaPlayer1.playState == WMPPlayState.wmppsMediaEnded || axWindowsMediaPlayer1.playState == WMPPlayState.wmppsPaused)
            {
               
                Cursor.Show();
                this.Controls.Remove(axWindowsMediaPlayer1);
                this.Controls.Remove(panels[0]);
                Form1_Load(this, EventArgs.Empty);
                Label avlabf = new Label();
                avlabf.Visible = true;
                avlabf.Font = avfont;
                avlabf.Size = new Size(400, 100);
                avlabf.Location = new Point(500, 100);
                avlabf.Text = "KORKI TRIVIA";
                avlabf.ForeColor = Color.Blue;
                avlabf.BackColor = Color.Transparent;
                avlab = avlabf;
                this.Controls.Add(avlab);
                globalplyer.URL = pathoffolder;//to mp3
                globalplyer.controls.play();


            }
        }



        public class loginclass {
            public loginclass(string user, string pass)
            {
                this.username = user;
                this.password = pass;
            }
            public string username;
            public string password;

        };
        public class signupclass
        {
            public signupclass(string user, string pass, string em)
            {
                this.email = em;
                this.password = pass;
                this.username = user;
            }
            public string username;
            public string password;
            public string email;
        };
        public static String ToBinary(Byte[] data)
        {
            return string.Join(" ", data.Select(byt => Convert.ToString(byt, 2).PadLeft(8, '0')));
        }
        public static int ret_size(byte[] data)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(data);

            int i = BitConverter.ToInt32(data, 0);
            return i;
        }
        public class response
        {
            public response()
            {
                //
            }
            public bool LoginStatus;
        };
        public class responses
        {
            public responses()
            {

            }
            public bool status;
        };
        public class roomData
        {
            public roomData(string name, string maxUsers, string questioncount, string questiontime)
            {
                roomName = name;
                try
                {
                    this.maxUsers = int.Parse(maxUsers);
                    this.questionCount = int.Parse(questioncount);
                    this.answerTimeout = int.Parse(questiontime);
                }
                catch
                {

                }
                }
            public string roomName;
            public int maxUsers, questionCount, answerTimeout;

        };
        public void senddatad(byte [] datajson, byte stamp, NetworkStream stream)
        {
            int len = datajson.Length;
            byte[] bytes = new byte[4];
            unchecked
            {
                bytes[0] = (byte)(len >> 24);
                bytes[1] = (byte)(len >> 16);
                bytes[2] = (byte)(len >> 8);
                bytes[3] = (byte)(len);
            }
            byte[] finalbuf = new byte[len + 5];
            finalbuf[0] = stamp;
            for (int i = 0; i < 4; i++)
            {
                finalbuf[i + 1] = bytes[i];
            }
            for (int i = 5; i < len + 5; i++)
            {
                finalbuf[i] = datajson[i - 5];
            }
            stream.Write(finalbuf, 0, len + 5);
        }
        public bool sendroomdata(string name, string maxUsers,string questioncount,string questiontime)
        {
            NetworkStream stream = client.GetStream();
            roomData rd = new roomData(name, maxUsers, questioncount, questiontime);
            var json2 = new JavaScriptSerializer().Serialize(rd);
            byte[] datajson = Encoding.ASCII.GetBytes(json2);
            byte stamp = 3;
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            statclass statclass = new JavaScriptSerializer().Deserialize<statclass>(json_st);
            if (statclass.status == 1)
            {
                pictureBox1.Visible = false;
                return true;

                //ask for romm data
            }
            else
            {
                return false;
            }

        }
        public class statclass
        {

            public int status;
        };
         public class roomdatastruct
        {
            public int id;
            public string name;
            public int maxplayers;
            public int timePerQuestion;
            public int questionCount;
            public int isactive;
            public int currplayers;
        };
        public class joinroomstruct
        {
            public int roomid;
        };
        public bool logout()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 7;
            byte[] jsonbytes = new byte[0];
            senddatad(jsonbytes, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            statclass statclass = new JavaScriptSerializer().Deserialize<statclass>(json_st);
            if (statclass.status == 1)
            {
                return true;
                //ask for romm data
            }
            else
            {
                return false;
            }
        }
        public void LeaveGame()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 15;
            byte[] jsonbytes = new byte[0];
            senddatad(jsonbytes, stamp, stream);
        }
        public bool joinRoomById(int id)
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 5;//join room     
            joinroomstruct joinroomstruct = new joinroomstruct();
            joinroomstruct.roomid = id;
            var json = new JavaScriptSerializer().Serialize(joinroomstruct);
            byte[] jsonbytes = Encoding.ASCII.GetBytes(json);
            senddatad(jsonbytes, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            statclass statclass = new JavaScriptSerializer().Deserialize<statclass>(json_st);
            if (statclass.status == 1)
            {
                pictureBox1.Visible = false;
                return true;
                //ask for romm data
            }
            else
            {
                return false;
            }
        }

        public List<string> getplayersbyId(int id)
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 6;
            joinroomstruct joinroomstruct = new joinroomstruct();
            joinroomstruct.roomid = id;
            var json = new JavaScriptSerializer().Serialize(joinroomstruct);
            Byte[] jsonbytes = Encoding.ASCII.GetBytes(json);
            senddatad(jsonbytes, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            List<playernamec> l = new JavaScriptSerializer().Deserialize<List<playernamec>>(json_st);
            List<string> ls = new List<string>();
            foreach(playernamec entry in l)
            {
                ls.Add(entry.playerName);
            }
            return ls;
         }
        public List<roomdatac> GetRooms()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 4;
            byte [] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
          var json_st = Encoding.ASCII.GetString(conarr);
            string[] twok = json_st.Split('[');
            twok[1] = '[' + twok[1];
            
            statclass statclass = new JavaScriptSerializer().Deserialize<statclass>(twok[0]);
            if(statclass.status == 1)
            {
                List<roomdatac> rds = new JavaScriptSerializer().Deserialize<List<roomdatac>>(twok[1]);
                return rds;
            }
            List<roomdatac> rddd = new List<roomdatac>();
            return rddd;
               
            
        }
        public bool closeRoom()
        {
            NetworkStream stream= client.GetStream();
            byte stamp = 9;
            byte[] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            return true;
        }
        public class answer
        {
            public int answerid;
        }
        public int getCorrectAnswerPlusSubmit(int id)
        {
            answer answer1 = new answer();
            answer1.answerid = id;
            var json = new JavaScriptSerializer().Serialize(answer1);
            NetworkStream stream = client.GetStream();
            byte stamp = 13;
            byte[] datajson = Encoding.ASCII.GetBytes(json);
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            answer answer2 = new JavaScriptSerializer().Deserialize<answer>(json_st);
            return (answer2.answerid);
        }
        public class helperjsonobject
        {
            public int status;
            public string question;
        }
        public GetQuestionStruct getQuestionFromServer()
        {
            NetworkStream stream = client.GetStream();
            byte stamp  = 12;
            byte[] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            string firstjson = json_st.Substring(0,json_st.IndexOf("{space}"));
            string second_json = json_st.Substring(json_st.IndexOf("{space}") + 7);
            helperjsonobject helperjsonobj = Newtonsoft.Json.JsonConvert.DeserializeObject<helperjsonobject>(firstjson);
            Dictionary<int,string> dict =Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int,string>>(second_json);
            GetQuestionStruct gqr = new GetQuestionStruct();
            gqr.answers = dict;
            gqr.status = helperjsonobj.status;
            gqr.question = helperjsonobj.question;
            return gqr;


            
        }
        
        public playerResultWITHstatus we_are_in_the_endgame_now()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 14;
            byte[] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);
            string[] twok = json_st.Split('[');
            twok[1] = '[' + twok[1];
            statclass sc = new JavaScriptSerializer().Deserialize<statclass>(twok[0]);
            if(sc.status == 1)
            {
                playerResultWITHstatus prws = new playerResultWITHstatus();
                prws.status = 1;
                List<playerResult> plist = Newtonsoft.Json.JsonConvert.DeserializeObject<List<playerResult>>(twok[1]);
                prws.results = plist;
                return prws;

            }
            else
            {
                playerResultWITHstatus prws = new playerResultWITHstatus();
                prws.status = 0;
                prws.results = new List<playerResult>();
                return prws;
            }
        }
        public getRoomState getdataforroom()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 11;
            byte[] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            var json_st = Encoding.ASCII.GetString(conarr);

            getRoomState grs = new JavaScriptSerializer().Deserialize<getRoomState>(json_st);
            
            return grs;
        }
        public bool startRoom()
        {
            NetworkStream stream = client.GetStream();
            byte stamp = 10;
            byte[] datajson = new byte[0];
            senddatad(datajson, stamp, stream);
            byte[] buf = new byte[5];
            stream.Read(buf, 0, 5);
            int sersize = ret_size(buf);
            byte[] conarr = new byte[sersize];
            stream.Read(conarr, 0, sersize);
            return true;
        }
        private void L_Click(object sender, EventArgs e)
        {
            auidoeffect.controls.play();
            string username = "";
            string password = "";
            string email = "";
            bool islogin;
            bool cflag = true;
            bool correct = false;
            while (correct == false)
            {
                loginForm loginform1;
                this.Hide();
                if (cflag == false)
                {
                    loginform1 = new loginForm("wrong password or username");
                }
                else
                {
                    loginform1 = new loginForm("");
                }
                    if (loginform1.ShowDialog() == DialogResult.OK)
                    {
                        this.Show();
                        username = loginform1.username;
                        password = loginform1.password;
                        islogin = loginform1.isLogin;
                        if (!islogin)
                            email = loginform1.email;
                    if(loginform1.isgamil)
                    {
                        NetworkStream stream = client.GetStream();
                        byte[] em = Encoding.ASCII.GetBytes(email);
                        int data = em.Length;
                        byte[] bytes = new byte[4];
                        unchecked
                        {
                            bytes[0] = (byte)(data >> 24);
                            bytes[1] = (byte)(data >> 16);
                            bytes[2] = (byte)(data >> 8);
                            bytes[3] = (byte)(data);
                        }
                        byte[] final_buf = new byte[data + 5];
                        final_buf[0] = 80;
                        for (int i = 0; i < 4;i++)
                        {
                            final_buf[i + 1] = bytes[i];
                        }
                        for(int i =5;i<data + 5;i++)
                        {
                            final_buf[i] = em[i - 5];
                        }
                        stream.Write(final_buf, 0, data + 5);
                    }
                    else if (islogin)
                    {
                        loginclass lc = new loginclass(username, password);
                        var json2 = new JavaScriptSerializer().Serialize(lc);
                        byte[] jsonbytes = Encoding.ASCII.GetBytes(json2);
                        var binarystr = ToBinary(jsonbytes);
                        int data = binarystr.Length;
                        byte[] bytes = new byte[4];
                        unchecked
                        {
                            bytes[0] = (byte)(data >> 24);
                            bytes[1] = (byte)(data >> 16);
                            bytes[2] = (byte)(data >> 8);
                            bytes[3] = (byte)(data);
                        }
                        byte c = 1;
                        byte[] final = new byte[5 + data];
                        final[0] = c;
                        for(int i  = 0; i<4;i++)
                        {
                            final[i + 1] = bytes[i];
                        }
                        for(int i =0;i<data;i++)
                        {
                            final[i + 5] = (byte)binarystr[i];
                        }
                        NetworkStream stream;

                        
                        while (true)
                        {
                            try
                            {
                                stream = client.GetStream();
                                break;
                            }
                            catch
                            {
                                try
                                {
                                    client = new TcpClient();
                                    client.Connect("127.0.0.1", 6743);
                                }
                                catch
                                {

                                }
                            }

                        }
                        stream.Write(final, 0, 5 + data);
                        byte [] buf = new byte[5];
                        stream.Read(buf, 0, 5);
                        int asize = 0;
                        for(int i =1;i<5;i++)
                        {
                            buf[i] -= 48;
                        }
                        if (buf[0] == '1')
                        {
                            asize = ret_size(buf);
                        }
                  
                        byte[] conarr = new byte[asize];
                        stream.Read(conarr, 0, asize);
                        var server_json = System.Text.Encoding.Default.GetString(conarr);

                        response re = new response();
                        if (server_json.Contains("Status"))
                        {
                            if (conarr[asize -1] == 1)
                            {
                                re.LoginStatus = true;
                            }
                            else
                            {
                                re.LoginStatus = false;
                            }

                        }
                
                        if (re.LoginStatus == true)
                        {
                            correct = true;
                        }
                        else
                        {
                            correct = false;
                        }
                        cflag = false;
                    }
                   // correct = true;
                   else if(!islogin)
                    {
                        NetworkStream stream;
                        while (true)
                        {
                            try

                            {
                                stream = client.GetStream();
                                break;
                            }
                            catch
                            {
                                client = new TcpClient();
                                client.Connect("127.0.0.1", 6743);
                            }
                        }

                        signupclass sc = new signupclass(username, password, email);
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(sc);
                        byte[] datajson = Encoding.ASCII.GetBytes(json);
                        byte stamp = 2;
                        int len = datajson.Length;
                        byte[] bytes = new byte[4];
                        unchecked
                        {
                            bytes[0] = (byte)(len >> 24);
                            bytes[1] = (byte)(len >> 16);
                            bytes[2] = (byte)(len >> 8);
                            bytes[3] = (byte)(len);
                        }
                        byte[] finalbuf = new byte[len + 5];
                        finalbuf[0] = stamp;
                        for(int i =0;i<4;i++)
                        {
                            finalbuf[i + 1] = bytes[i];
                        }
                        for(int i = 5;i< len +5;i++)
                        {
                            finalbuf[i] = datajson[i - 5];
                        }
                        stream.Write(finalbuf, 0, len + 5);
                        byte[] buf = new byte[5];
                        stream.Read(buf, 0, 5);
                        int sersize = ret_size(buf);
                        byte[] conarr = new byte[sersize];
                        stream.Read(conarr, 0, sersize);
                        var json_st = Encoding.ASCII.GetString(conarr);
                        responses rrr = new responses();
                        rrr = Newtonsoft.Json.JsonConvert.DeserializeObject<responses>(json_st);
                        if (rrr.status == true)
                        {
                            correct = true;
                        }
                        else
                        {
                            correct = false;
                        }
                    }
                }

                


            }
            userlabel.Text = username;
            userlabel.Visible = true;
            panels[0].Visible = false;
            this.Controls.Remove(panels[0]);
            MAINmenu mAI = new MAINmenu(this);
        }
        public void killavlab()
        {
            // this.Controls.Remove(avlab);
            avlab.Visible = false;
        }
        private void label1_Enter(object sender, EventArgs e)
        {
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               40,
               FontStyle.Underline,
               GraphicsUnit.Pixel);
            Label L = (Label)sender;
            L.Font= font;
        }

        private void label1_Leave(object sender, EventArgs e)
        {
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               24,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            Label L = (Label)sender;
            L.Font = font;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            bool flag = false ;
            if(e.KeyCode == Keys.Down)
            {
               for(int i =0; i<menulabels.Length;i++)
                {
                    if(menulabels[i].Focused)
                    {
                        flag = true;
                        //label1_Leave(menulabels[i], KeyEventArgs.Empty);

                        if(i == menulabels.Length -1 )
                        {
                            menulabels[0].Focus();
                        }
                        else
                        {
                            menulabels[i +1].Focus();
                        }
                        break;
                    }
                }
               if(!flag)
                {
                    menulabels[0].Focus();
                }
            }
            else if(e.KeyCode == Keys.Up)
            {
                for (int i = 0; i < menulabels.Length; i++)
                {
                    if (menulabels[i].Focused)
                    {
                        //label1_Leave(menulabels[i], KeyEventArgs.Empty);
                        if (i == 0)
                        {
                            menulabels[menulabels.Length - 1].Focus();
                        }
                        else
                        {
                            menulabels[i - 1].Focus();
                        }
                        break;
                    }
                }
            }
           
            else if(e.KeyCode == Keys.Enter)
            {
                if (panels[0].Visible)
                {
                    for (int i = 0; i < 4 ; i++)
                    {
                        if (menulabels[i].Focused == true)
                        {
                            switch (i)
                            {
                                case 0:
                                    L_Click(menulabels[i], KeyEventArgs.Empty);
                                    break;
                                case 1:
                                    Options_Click(menulabels[i], KeyEventArgs.Empty);
                                    break;
                                case 2:
                                    Info_Click(menulabels[i], KeyEventArgs.Empty);
                                    break;
                                case 3:
                                    Quit_Click(menulabels[i], KeyEventArgs.Empty);
                                    break;
                            }
                            break;

                        }
                    }
                }
                else if(panels[1] != null &&panels[1].Visible)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (menulabels[i].Focused == true)
                        {
                            switch (i)
                            {
                                case 0:
                                    Volume_Click(menulabels[i], EventArgs.Empty);
                                    break;
                                case 1:
                                    Changesong_Click(menulabels[i], EventArgs.Empty);
                                    break;
                                case 2:
                                    Back_Click(menulabels[i], EventArgs.Empty);
                                    break;
                            }

                        }
                    }
                }
                   
            }
            else if(e.KeyCode == Keys.Right)
            {
                try
                {
                    if (panels[1].Visible == true)
                    {
                        if (menulabels[0].Focused)
                        {
                            textBox1.Focus();
                        }
                    }
                }
                catch
                {

                }
            }
            else if(e.KeyCode == Keys.Escape)
            {
                if(panels[1] != null  && panels[1].Visible == true)
                {
                    if(textBox1.Focused)
                    {
                        menulabels[0].Focus();
                    }
                }
                if(textBox1.Focused == false)
                {
                    this.WindowState = FormWindowState.Normal;
                    timer1.Start();
                }
                
            }
            
         
            if(e.KeyCode == Keys.W && e.KeyCode == Keys.Control)
            {
                Application.Exit();
            }
            
        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            Random r = new Random();
            this.BackColor = Color.FromArgb(r.Next(256), r.Next(256), r.Next(256));
            try
            {
                panels[0].BackColor = this.BackColor;
                panels[0].Size = new Size(r.Next(), r.Next());
                this.Size = panels[0].Size;
                panels[1].BackColor = this.BackColor;
               
            }
            catch
            {
                
            }
        }

        private void axWindowsMediaPlayer1_Enter_1(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Size = new Size(this.Width,this.Height);
        }

        private void axWindowsMediaPlayer1_EndOfStream(object sender, AxWMPLib._WMPOCXEvents_EndOfStreamEvent e)
        {
            axWindowsMediaPlayer1.close();
        }

        private void axWindowsMediaPlayer1_Leave(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.close();
        }

        private void axWindowsMediaPlayer1_ClickEvent(object sender, AxWMPLib._WMPOCXEvents_ClickEvent e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            client.Close();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            globalplyer.settings.volume = trackBar1.Value * 10; 
        }
    }
}
