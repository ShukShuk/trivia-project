﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BunifuAnimatorNS;
using Bunifu.Framework.Lib;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace WindowsFormsApp23
{
    
    public partial class loginForm : Form
    {
        public bool isLogin { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public bool isgamil { get; set; }
        bool islogin = true;
        string err;
        public loginForm(string error)
        {
            InitializeComponent();
            err = error;
            passwordtb.PasswordChar = '*';
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            usernametb.Visible = false;
            passwordtb.Visible = false;
            emailtb.Visible = true;
            button1.Click -= button1_Click;
            button1.Click += new_click;
        }
        private void new_click(object sender, EventArgs e)
        {
            isgamil = true;
            email = emailtb.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
            //MailMessage mm = new MailMessage("triviashimim166@gmail.com", emailtb.Text, "your password", "your password is");
            //SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            //smtp.Port = 587;
            //smtp.EnableSsl = true;
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtp.UseDefaultCredentials = false;
            //smtp.Timeout = 30 * 1000;
            //smtp.Credentials = new System.Net.NetworkCredential("triviashimim166@gmail.com,", "magshimagshi");
            //smtp.Send(mm);
        }
        private void loginForm_Load(object sender, EventArgs e)
        {

            emailtb.TextChanged += Emailtb_TextChanged;
            this.Anchor =AnchorStyles.None;
            this.FormBorderStyle = FormBorderStyle.None;
            usernametb.Anchor = AnchorStyles.None;
            emailtb.Anchor = AnchorStyles.None;
            button1.Anchor = AnchorStyles.None;
            passwordtb.Anchor = AnchorStyles.None;
            linkLabel1.Anchor = AnchorStyles.None;
            linkLabel2.Anchor = AnchorStyles.None;
            linkLabel3.Anchor = AnchorStyles.None;
            this.WindowState = FormWindowState.Maximized;
            linkLabel3.Visible = false;
            if(err != "")
            {
                linkLabel3.Visible = true;
                linkLabel3.Text = "error";
            }
        }

        private void Emailtb_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(islogin)
            {
                isLogin = true;
                username = usernametb.Text;
                password = passwordtb.Text;
                email = "";
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                isLogin = false;
                username = usernametb.Text;
                password = passwordtb.Text;
                email = emailtb.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            emailtb.Visible = true;
            islogin = false;
        }

       
    }
}
