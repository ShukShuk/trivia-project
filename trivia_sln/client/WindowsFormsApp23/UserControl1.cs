﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
namespace WindowsFormsApp23
{
    public partial class UserControl1 : UserControl
    {
        List<Label> _players = new List<Label>();
        public UserControl1()
        {
            InitializeComponent();
        }
        public void updateList(List<string> ls, string username)
        {
            foreach(Label entry in _players)
            {
                flowLayoutPanel1.Controls.Remove(entry);
            }
            _players.Clear();
            addtolist(ls, username);
        }
        void UserControl1_Load(object sender, EventArgs e)
        {
            flowLayoutPanel1.Width += 200;
            flowLayoutPanel1.Location = new Point(this.Width / 2 - 200, this.Height / 2 - 200);
            flowLayoutPanel1.WrapContents = false;
            flowLayoutPanel1.AutoScroll = true ;
            flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
            flowLayoutPanel1.BackColor = Color.Gray;
        }
        public void addbutton(MetroFramework.Controls.MetroButton b)
        {
            b.BackColor = Color.Gray;
            if (b.Text == "Leave")
            {
                b.Location = new Point(flowLayoutPanel1.Location.X,flowLayoutPanel1.Location.Y + flowLayoutPanel1.Height);
                b.Size = new Size(flowLayoutPanel1.Width / 2, b.Height);
                b.AutoSize = true;
            }
            else if(b.Text == "Start")
            {
                b.Location = new Point(flowLayoutPanel1.Location.X + (flowLayoutPanel1.Width / 2) + 50,flowLayoutPanel1.Height + flowLayoutPanel1.Location.Y);
                b.Size = new Size(flowLayoutPanel1.Width / 2, b.Height);
                b.AutoSize = true;
                
                    }
            this.Controls.Add(b);
        }
        public void resizepic()
        {
            var pic = new Bitmap(this.BackgroundImage, this.Width, this.Height);
            this.BackgroundImage = pic;
        }
        public void addtolist(List<string> ls, string username)
        {
            foreach (string entry in ls)
            {
                Label l = new Label();
                l.Text = entry;
                l.AutoSize = true;
                FontFamily family = new FontFamily("Arial");
                l.Font = new Font(family, 30, FontStyle.Bold);
                if(entry == username)
                {
                    l.BackColor = Color.Blue;
                }
                else
                {
                    l.BackColor = Color.Red;
                }
                l.Width = flowLayoutPanel1.Width;
                flowLayoutPanel1.Controls.Add(l);
                _players.Add(l);
                flowLayoutPanel1.ScrollControlIntoView(l);
            }

        }

        private void metroProgressSpinner1_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
